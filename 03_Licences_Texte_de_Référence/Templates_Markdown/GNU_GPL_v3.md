**In English:**

{Product Name} is {description}.  
{Product Name} is a device of ToucheLibre project.

The aim of ToucheLibre project is to design ergonomic human / machine interfaces with following values: beautiful aesthetic, respect of health user, fight against planned obsolescence and ecologic responsability.

Copyright © Lilian Tribouilloy 2020

All the software part of this product (all the source files in this directory) are under the **GNU General Public License version 3.0 or later version**.

In according to the SPDX specification from <https://spdx.org/licenses/>, the identifiers are:  
- SPDX-FileCopyrightText: 2020 Lilian Tribouilloy <lilian@touchelibre.fr>
- SPDX-License-Identifier: GPL-3.0-or-later

You may redistribute and modify this source and make products using it under the terms of the GNU GPL v3 or later version.

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

The original text of this licence is in the FSF’s web site : <https://www.gnu.org/licenses/gpl-3.0.html>

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

Source location of ToucheLibre project:  
<http://touchelibre.fr> and <https://gitlab.com/touchelibre>


______________________________________________________________

**En Français :**

{Product Name} est {description}.
{Product Name} est un appareil du projet ToucheLibre.

L'objectif du projet ToucheLibre est de concevoir des interfaces homme / machine ergonomiques avec les valeurs suivantes : belle esthétique, respect de la santé de l'utilisateur, lutte contre l'obsolescence programmée et responsabilité écologique.

Tous droits réservés © Lilian Tribouilloy 2020

Toute la partie logiciel du projet (tous les fichiers sources présents dans ce répertoire) est placé sous **GNU General Public License version 3.0 ou toute version ultérieure**.

En accord avec la spécification SPDX depuis <https://spdx.org/licenses/>, les identifiants sont :  
- SPDX-FileCopyrightText: 2020 Lilian Tribouilloy <lilian@touchelibre.fr>
- SPDX-License-Identifier: GPL-3.0-or-later

Ce programme est un logiciel libre: _Vous_ pouvez le redistribuer et / ou le modifier selon les termes de la Licence Publique Générale GNU telle que publiée par la Free Software Foundation, soit la version 3 de la Licence, soit (à votre choix) toute version ultérieure.

Le texte originale de cette licence se trouve dans le site web de la FSF : <https://www.gnu.org/licenses/gpl-3.0.html>. Pour votre compréhension, une traduction non‑officielle en français, mais sans valeur juridique, est également proposé à la fin de ce fichier.

Ce programme est distribué dans l'espoir qu'il sera utile, mais SANS AUCUNE GARANTIE; sans même la garantie implicite de QUALITÉ MARCHANDE ou D'ADAPTATION À UN USAGE PARTICULIER. Voir la licence publique générale GNU pour plus de détails.

Emplacement des sources du projet ToucheLibre :  
<http://touchelibre.fr> et <https://gitlab.com/touchelibre>


______________________________________________________________

> You can find bellow a copy of this licence just for your information. And in according to the requirements from the User Guide.
> 
> This copy have some esthetical modifications so only original text in FSF web site is applicable.
> 
> These esthetical modifications are :  
> * Translation in markdown format,  
> * Words in definition (§0) are in emphasis in all the text,  
> * Some modifications to improve the typography.



# GNU GENERAL PUBLIC LICENSE — Version 3, 29 June 2007

_Copyright_ © 2007 Free Software Foundation, Inc. <http://fsf.org/>
Everyone is permitted to copy and distribute verbatim copies of this license document, but changing it is not allowed.


## Preamble

The GNU General Public License is a free, copyleft license for software and other kinds of works.

The licenses for most software and other practical works are designed to take away your freedom to share and change the works. By contrast, the GNU General Public License is intended to guarantee your freedom to share and change all versions of a program to make sure it remains free software for all its users. We, the Free Software Foundation, use the GNU General Public License for most of our software; it applies also to any other work released this way by its authors. You can apply it to your programs, too.

When we speak of free software, we are referring to freedom, not price. Our General Public Licenses are designed to make sure that you have the freedom to distribute copies of free software (and charge for them if you wish), that you receive _Source Code_ or can get it if you want it, that you can change the software or use pieces of it in new free programs, and that you know you can do these things.

To protect your rights, we need to prevent others from denying you these rights or asking you to surrender the rights. Therefore, you have certain responsibilities if you distribute copies of the software, or if you _Modify_ it: responsibilities to respect the freedom of others.

For example, if you distribute copies of such a program, whether gratis or for a fee, you must pass on to the recipients the same freedoms that you received. You must make sure that they, too, receive or can get the _Source Code_. And you must show them these terms so they know their rights.

Developers that use the GNU GPL protect your rights with two steps: **(1)** assert _Copyright_ on the software, and **(2)** offer you this _License_ giving you legal permission to copy, distribute and/or _Modify_ it.

For the developers’ and authors’ protection, the GPL clearly explains that there is no warranty for this free software. For both users’ and authors’ sake, the GPL requires that _Modified Versions_ be marked as changed, so that their problems will not be attributed erroneously to authors of previous versions.

Some devices are designed to deny users access to install or run _Modified Versions_ of the software inside them, although the manufacturer can do so. This is fundamentally incompatible with the aim of protecting users’ freedom to change the software. The systematic pattern of such abuse occurs in the area of products for individuals to use, which is precisely where it is most unacceptable. Therefore, we have designed this version of the GPL to prohibit the practice for those products. If such problems arise substantially in other domains, we stand ready to extend this provision to those domains in future versions of the GPL, as needed to protect the freedom of users.

Finally, every program is threatened constantly by software patents. States should not allow patents to restrict development and use of software on general‑purpose computers, but in those that do, we wish to avoid the special danger that patents applied to a free program could make it effectively proprietary. To prevent this, the GPL assures that patents cannot be used to render the program non‑free.

The precise terms and conditions for copying, distribution and modification follow.


## TERMS AND CONDITIONS

### 0. Definitions

This “_License_” refers to version 3 of the GNU General Public License.

“_Copyright_” also means copyright-like laws that apply to other kinds of works, such as semiconductor masks.

The “_Program_” refers to any copyrightable work licensed under this _License_. Each licensee is addressed as “_You_”. “_Licensees_” and “_Recipients_” may be individuals or organizations.

To “_Modify_” a work means to copy from or adapt all or part of the work in a fashion requiring _Copyright_ permission, other than the making of an exact copy. The resulting work is called a “_Modified Version_” of the earlier work or a work “based on” the earlier work.

A “_Covered Work_” means either the unmodified _Program_ or a work based on the _Program_.

To “_Propagate_” a work means to do anything with it that, without permission, would make you directly or secondarily liable for infringement under applicable _Copyright_ law, except executing it on a computer or _Modifying_ a private copy. Propagation includes copying, distribution (with or without modification), making available to the public, and in some countries other activities as well.

To “_Convey_” a work means any kind of propagation that enables other parties to make or receive copies. Mere interaction with a user through a computer network, with no transfer of a copy, is not _Conveying_.

An interactive user interface displays “_Appropriate Legal Notices_” to the extent that it includes a convenient and prominently visible feature that **(1)** displays an appropriate _Copyright_ notice, and **(2)** tells the user that there is no warranty for the work (except to the extent that warranties are provided), that licensees may _Convey_ the work under this License, and how to view a copy of this License. If the interface presents a list of user commands or options, such as a menu, a prominent item in the list meets this criterion.


### 1. Source Code

The “_Source Code_” for a work means the preferred form of the work for making modifications to it. “_Object code_” means any non-source form of a work.

A “_Standard Interface_” means an interface that either is an official standard defined by a recognized standards body, or, in the case of interfaces specified for a particular programming language, one that is widely used among developers working in that language.

The “_System Libraries_” of an executable work include anything, other than the work as a whole, that **(a)** is included in the normal form of packaging a Major Component, but which is not part of that Major Component, and **(b)** serves only to enable use of the work with that Major Component, or to implement a _Standard Interface_ for which an implementation is available to the public in _Source Code_ form. A “_Major Component_”, in this context, means a major essential component (kernel, window system, and so on) of the specific operating system (if any) on which the executable work runs, or a compiler used to produce the work, or an object code interpreter used to run it.

The “_Corresponding Source_” for a work in object code form means all the _Source Code_ needed to generate, install, and (for an executable work) run the object code and to _Modify_ the work, including scripts to control those activities.  However, it does not include the work’s _System Libraries_, or general-purpose tools or generally available free programs which are used unmodified in performing those activities but which are not part of the work. For example, _Corresponding Source_ includes interface definition files associated with source files for the work, and the _Source Code_ for shared libraries and dynamically linked subprograms that the work is specifically designed to require, such as by intimate data communication or control flow between those subprograms and other parts of the work.

The _Corresponding Source_ need not include anything that users can regenerate automatically from other parts of the _Corresponding Source_.

The _Corresponding Source_ for a work in _Source Code_ form is that same work.


### 2. Basic Permissions

All rights granted under this License are granted for the term of _Copyright_ on the _Program_, and are irrevocable provided the stated conditions are met. This _License_ explicitly affirms your unlimited permission to run the unmodified _Program_. The output from running a _Covered Work_ is covered by this License only if the output, given its content, constitutes a _Covered Work_. This _License_ acknowledges your rights of fair use or other equivalent, as provided by _Copyright_ law.

You may make, run and _Propagate_ _Covered Works_ that you do not _Convey_, without conditions so long as your license otherwise remains in force. You may _Convey_ _Covered Works_ to others for the sole purpose of having them make modifications exclusively for you, or provide you with facilities for running those works, provided that you comply with the terms of this License in _Conveying_ all material for which you do not control _Copyright_. Those thus making or running the _Covered Works_ for you must do so exclusively on your behalf, under your direction and control, on terms that prohibit them from making any copies of your _Copyrighted_ material outside their relationship with you.

_Conveying_ under any other circumstances is permitted solely under the conditions stated below. Sublicensing is not allowed; section **10** makes it unnecessary.


### 3. Protecting Users’ Legal Rights From Anti-Circumvention Law

No _Covered Work_ shall be deemed part of an effective technological measure under any applicable law fulfilling obligations under [article 11](https://www.wipo.int/edocs/pubdocs/en/wipo_pub_226.pdf) of the World Intellectual Property Organization Copyright Treaty (WIPO Copyright Treaty or WCT) adopted on 20 December 1996, or similar laws prohibiting or restricting circumvention of such measures.

When you _Convey_ a _Covered Work_, you waive any legal power to forbid circumvention of technological measures to the extent such circumvention is effected by exercising rights under this License with respect to the _Covered Work_, and you disclaim any intention to limit operation or modification of the work as a means of enforcing, against the work’s users, your or third parties’ legal rights to forbid circumvention of technological measures.


### 4. Conveying Verbatim Copies

You may _Convey_ verbatim copies of the _Program_’s _Source Code_ as you receive it, in any medium, provided that you conspicuously and appropriately publish on each copy an appropriate _Copyright_ notice; _keep intact all notices_ stating that this License and any non-permissive terms added in accord with section **7** apply to the code; _keep intact all notices_ of the absence of any warranty; and give all recipients a copy of this License along with the _Program_.

You may charge any price or no price for each copy that you _Convey_, and you may offer support or warranty protection for a fee.


### 5. Conveying Modified Source Versions

You may _Convey_ a work based on the _Program_, or the modifications to produce it from the _Program_, in the form of _Source Code_ under the terms of section **4**, provided that you also meet all of these conditions:
1. The work must carry prominent notices stating that you _modified_ it, and giving a relevant date.
2. The work must carry prominent notices stating that it is released under this License and any conditions added under section **7**. This requirement _Modifies_ the requirement in section **4** to “_keep intact all notices_”.
3. You must license the entire work, as a whole, under this License to anyone who comes into possession of a copy. This _License_ will therefore apply, along with any applicable section **7** additional terms, to the whole of the work, and all its parts, regardless of how they are packaged. This _License_ gives no permission to license the work in any other way, but it does not invalidate such permission if you have separately received it.
4. If the work has interactive user interfaces, each must display _Appropriate Legal Notices_; however, if the _Program_ has interactive interfaces that do not display _Appropriate Legal Notices_, your work need not make them do so.

A compilation of a _Covered Work_ with other separate and independent works, which are not by their nature extensions of the _Covered Work_, and which are not combined with it such as to form a larger program, in or on a volume of a storage or distribution medium, is called an “_aggregate_” if the compilation and its resulting _Copyright_ are not used to limit the access or legal rights of the compilation’s users beyond what the individual works permit.  Inclusion of a _Covered Work_ in an _aggregate_ does not cause this _License_ to apply to the other parts of the _aggregate_.


### 6. Conveying Non-Source Forms

You may _Convey_ a _Covered Work_ in object code form under the terms of sections **4** and **5**, provided that you also _Convey_ the machine-readable _Corresponding Source_ under the terms of this _License_, in one of these ways:
1. _Convey_ the object code in, or embodied in, a physical product (including a physical distribution medium), accompanied by the _Corresponding Source_ fixed on a durable physical medium customarily used for software interchange.
2. _Convey_ the object code in, or embodied in, a physical product (including a physical distribution medium), accompanied by a written offer, valid for at least three years and valid for as long as you offer spare parts or customer support for that product model, to give anyone who possesses the object code either **(1)** a copy of the _Corresponding Source_ for all the software in the product that is covered by this _License_, on a durable physical medium customarily used for software interchange, for a price no more than your reasonable cost of physically performing this _Conveying_ of source, or **(2)** access to copy the _Corresponding Source_ from a network server at no charge.
3. _Convey_ individual copies of the object code with a copy of the written offer to provide the _Corresponding Source_. This alternative is allowed only occasionally and noncommercially, and only if you received the object code with such an offer, in accord with subsection **6.2**.
4. _Convey_ the object code by offering access from a designated place (gratis or for a charge), and offer equivalent access to the _Corresponding Source_ in the same way through the same place at no further charge. You need not require recipients to copy the _Corresponding Source_ along with the object code. If the place to copy the object code is a network server, the _Corresponding Source_ may be on a different server (operated by you or a third party) that supports equivalent copying facilities, provided you maintain clear directions next to the object code saying where to find the _Corresponding Source_. Regardless of what server hosts the _Corresponding Source_, you remain obligated to ensure that it is available for as long as needed to satisfy these requirements.
5. _Convey_ the object code using peer-to-peer _Transmission_, provided you inform other peers where the object code and _Corresponding Source_ of the work are being offered to the general public at no charge under subsection **6.4**.

A separable portion of the object code, whose _Source Code_ is excluded from the _Corresponding Source_ as a System Library, need not be included in _Conveying_ the object code work.

A “_User Product_” is either **(1)** a “_Consumer Product_”, which means any tangible personal property which is normally used for personal, family, or household purposes, or **(2)** anything designed or sold for incorporation into a dwelling. In determining whether a product is a _Consumer Product_, doubtful cases shall be resolved in favor of coverage. For a particular product received by a particular user, “normally used” refers to a typical or common use of that class of product, regardless of the status of the particular user or of the way in which the particular user actually uses, or expects or is expected to use, the product. A product is a _Consumer Product_ regardless of whether the product has substantial commercial, industrial or non-consumer uses, unless such uses represent the only significant mode of use of the product.

“_Installation Information_” for a _User Product_ means any methods, procedures, authorization keys, or other information required to install and execute _Modified Versions_ of a _Covered Work_ in that _User Product_ from a _Modified Version_ of its _Corresponding Source_.  The information must suffice to ensure that the continued functioning of the _Modified_ object code is in no case prevented or interfered with solely because modification has been made.

If you _Convey_ an object code work under this section in, or with, or specifically for use in, a _User Product_, and the _Conveying_ occurs as part of a transaction in which the right of possession and use of the _User Product_ is transferred to the recipient in perpetuity or for a fixed term (regardless of how the transaction is characterized), the _Corresponding Source_ _Conveyed_ under this section must be accompanied by the _Installation Information_.  But this requirement does not apply if neither you nor any third party retains the ability to install _Modified_ object code on the _User Product_ (for example, the work has been installed in ROM).

The requirement to provide _Installation Information_ does not include a requirement to continue to provide support service, warranty, or updates for a work that has been _Modified_ or installed by the recipient, or for the _User Product_ in which it has been _Modified_ or installed. Access to a network may be denied when the modification itself materially and adversely affects the operation of the network or violates the rules and protocols for communication across the network.

_Corresponding Source_ _Conveyed_, and _Installation Information_ provided, in accord with this section must be in a format that is publicly documented (and with an implementation available to the public in _Source Code_ form), and must require no special password or key for unpacking, reading or copying.


### 7. Additional Terms

“_Additional Permissions_” are terms that supplement the terms of this _License_ by making exceptions from one or more of its conditions. _Additional Permissions_ that are applicable to the entire _Program_ shall be treated as though they were included in this _License_, to the extent that they are valid under applicable law. If _Additional Permissions_ apply only to part of the _Program_, that part may be used separately under those permissions, but the entire _Program_ remains governed by this _License_ without regard to the _Additional Permissions_.

When you _Convey_ a copy of a _Covered Work_, you may at your option remove any _Additional Permissions_ from that copy, or from any part of it. (_Additional Permissions_ may be written to require their own removal in certain cases when you _Modify_ the work.) You may place _Additional Permissions_ on material, added by you to a _Covered Work_, for which you have or can give appropriate _Copyright_ permission.

Notwithstanding any other provision of this _License_, for material you add to a _Covered Work_, you may (if authorized by the _Copyright_ holders of that material) supplement the terms of this _License_ with terms:
1. Disclaiming warranty or limiting liability differently from the terms of sections **15** and **16** of this _License_; or
2. Requiring preservation of specified reasonable legal notices or author attributions in that material or in the _Appropriate Legal Notices_ displayed by works containing it; or
3. Prohibiting misrepresentation of the origin of that material, or requiring that _Modified Versions_ of such material be marked in reasonable ways as different from the original version; or
4. Limiting the use for publicity purposes of names of licensors or authors of the material; or
5. Declining to grant rights under trademark law for use of some trade names, trademarks, or service marks; or
6. Requiring indemnification of licensors and authors of that material by anyone who _Conveys_ the material (or _Modified Versions_ of it) with contractual assumptions of liability to the recipient, for any liability that these contractual assumptions directly impose on those licensors and authors.

All other non‑permissive additional terms are considered “_Further Restrictions_” within the meaning of section 10. If the _Program_ as youreceived it, or any part of it, contains a notice stating that it is governed by this _License_ along with a term that is a further restriction, you may remove that term. If a license document contains a further restriction but permits relicensing or _Conveying_ under this _License_, you may add to a _Covered Work_ material governed by the terms of that license document, provided that the further restriction does not survive such relicensing or _Conveying_.

If you add terms to a _Covered Work_ in accord with this section, you must place, in the relevant source files, a statement of the additional terms that apply to those files, or a notice indicating where to find the applicable terms.

Additional terms, permissive or non‑permissive, may be stated in the form of a separately written license, or stated as exceptions; the above requirements apply either way.


### 8. Termination

You may not _Propagate_ or _Modify_ a _Covered Work_ except as expressly provided under this _License_. Any attempt otherwise to _Propagate_ or _Modify_ it is void, and will automatically terminate your rights under this _License_ (including any patent licenses granted under the third paragraph of section **11**).

However, if you cease all violation of this _License_, then your license from a particular _Copyright_ holder is reinstated **(a)** provisionally, unless and until the _Copyright_ holder explicitly and finally terminates your license, and **(b)** permanently, if the _Copyright_ holder fails to notify you of the violation by some reasonable means prior to 60 days after the cessation.

Moreover, your license from a particular _Copyright_ holder is reinstated permanently if the _Copyright_ holder notifies you of the violation by some reasonable means, this is the first time you have received notice of violation of this _License_ (for any work) from that _Copyright_ holder, and you cure the violation prior to 30 days after your receipt of the notice.

Termination of your rights under this section does not terminate the licenses of parties who have received copies or rights from you under this _License_. If your rights have been terminated and not permanently reinstated, you do not qualify to receive new licenses for the same material under section **10**.


### 9. Acceptance Not Required for Having Copies

You are not required to accept this _License_ in order to receive or run a copy of the _Program_. Ancillary propagation of a _Covered Work_ occurring solely as a consequence of using peer-to-peer _Transmission_ to receive a copy likewise does not require acceptance. However, nothing other than this _License_ grants you permission to _Propagate_ or _Modify_ any _Covered Work_. These actions infringe _Copyright_ if you do not accept this _License_. Therefore, by _Modifying_ or propagating a _Covered Work_, you indicate your acceptance of this _License_ to do so.


### 10. Automatic Licensing of Downstream Recipients

Each time you _Convey_ a _Covered Work_, the recipient automatically receives a license from the original licensors, to run, _Modify_ and _Propagate_ that work, subject to this _License_. You are not responsible for enforcing compliance by third parties with this _License_.

An “_Entity Transaction_” is a transaction transferring control of an organization, or substantially all assets of one, or subdividing an organization, or merging organizations. If propagation of a _Covered Work_ results from an _Entity Transaction_, each party to that transaction who receives a copy of the work also receives whatever licenses to the work the party’s predecessor in interest had or could give under the previous paragraph, plus a right to possession of the _Corresponding Source_ of the work from the predecessor in interest, if the predecessor has it or can get it with reasonable efforts.

You may not impose any _Further Restrictions_ on the exercise of the rights granted or affirmed under this _License_. For example, you may not impose a license fee, royalty, or other charge for exercise of rights granted under this _License_, and you may not initiate litigation (including a cross-claim or counterclaim in a lawsuit) alleging that any patent claim is infringed by making, using, selling, offering for sale, or importing the _Program_ or any portion of it.


### 11. Patents

A “_Contributor_” is a _Copyright_ holder who authorizes use under this _License_ of the _Program_ or a work on which the _Program_ is based. The work thus licensed is called the _Contributor_’s “_Contributor Version_”.

A _Contributor_’s “_essential patent claims_” are all patent claims owned or controlled by the _Contributor_, whether already acquired or hereafter acquired, that would be infringed by some manner, permitted by this _License_, of making, using, or selling its _Contributor_ version, but do not include claims that would be infringed only as a consequence of further modification of the _Contributor Version_. For purposes of this definition, "control" includes the right to grant patent sublicenses in a manner consistent with the requirements of this _License_.

Each _Contributor_ grants you a non-exclusive, worldwide, royalty-free patent license under the _Contributor_’s _essential patent claims_, to make, use, sell, offer for sale, import and otherwise run, _Modify_ and _Propagate_ the contents of its _Contributor Version_.

In the following three paragraphs, a “_patent license_” is any express agreement or commitment, however denominated, not to enforce a patent (such as an express permission to practice a patent or covenant not to sue for patent infringement). To “_grant_” such a patent license to a party means to make such an agreement or commitment not to enforce a patent against the party.

If you _Convey_ a _Covered Work_, knowingly relying on a patent license, and the _Corresponding Source_ of the work is not available for anyone to copy, free of charge and under the terms of this _License_, through a publicly available network server or other readily accessible means, then you must either **(1)** cause the _Corresponding Source_ to be so available, or **(2)** arrange to deprive yourself of the benefit of the patent license for this particular work, or **(3)** arrange, in a manner consistent with the requirements of this _License_, to extend the patent license to downstream recipients. “_Knowingly relying_” means you have actual knowledge that, but for the patent license, your _Conveying_ the _Covered Work_ in a country, or your recipient’s use of the _Covered Work_ in a country, would infringe one or more identifiable patents in that country that you have reason to believe are valid.

If, pursuant to or in connection with a single transaction or arrangement, you _Convey_, or _Propagate_ by procuring conveyance of, a _Covered Work_, and grant a patent license to some of the parties receiving the _Covered Work_ authorizing them to use, _Propagate_, _Modify_ or _Convey_ a specific copy of the _Covered Work_, then the patent license you grant is automatically extended to all recipients of the _Covered Work_ and works based on it.

A patent license is “_Discriminatory_” if it does not include within the scope of its coverage, prohibits the exercise of, or is conditioned on the non-exercise of one or more of the rights that are specifically granted under this _License_. You may not _Convey_ a _Covered Work_ if you are a party to an arrangement with a third party that is in the business of distributing software, under which you make payment to the third party based on the extent of your activity of _Conveying_ the work, and under which the third party grants, to any of the parties who would receive the _Covered Work_ from you, a _Discriminatory_ patent license **(a)** in connection with copies of the _Covered Work_ _Conveyed_ by you (or copies made from those copies), or **(b)** primarily for and in connection with specific products or compilations that contain the _Covered Work_, unless you entered into that arrangement, or that patent license was granted, prior to 28 March 2007.

Nothing in this _License_ shall be construed as excluding or limiting any implied license or other defenses to infringement that may otherwise be available to you under applicable patent law.


### 12. No Surrender of Others’ Freedom

If conditions are imposed on you (whether by court order, agreement or otherwise) that contradict the conditions of this _License_, they do not excuse you from the conditions of this _License_. If you cannot _Convey_ a _Covered Work_ so as to satisfy simultaneously your obligations under this _License_ and any other pertinent obligations, then as a consequence you may not _Convey_ it at all. For example, if you agree to terms that obligate you to collect a royalty for further _Conveying_ from those to whom you _Convey_ the _Program_, the only way you could satisfy both those terms and this _License_ would be to refrain entirely from _Conveying_ the _Program_.


### 13. Use with the GNU Affero General Public License

Notwithstanding any other provision of this _License_, you have permission to link or combine any _Covered Work_ with a work licensed under version 3 of the GNU Affero General Public License into a single combined work, and to _Convey_ the resulting work. The terms of this _License_ will continue to apply to the part which is the _Covered Work_, but the special requirements of the GNU Affero General Public License, section **13**, concerning interaction through a network will apply to the combination as such.


### 14. Revised Versions of this License

The Free Software Foundation may publish revised and/or new versions of the GNU General Public License from time to time. Such new versions will be similar in spirit to the present version, but may differ in detail to address new problems or concerns.

Each version is given a distinguishing version number. If the _Program_ specifies that a certain numbered version of the GNU General Public License "or any later version" applies to it, you have the option of following the terms and conditions either of that numbered version or of any later version published by the Free Software Foundation. If the _Program_ does not specify a version number of the GNU General Public License, you may choose any version ever published by the Free Software Foundation.

If the _Program_ specifies that a proxy can decide which future versions of the GNU General Public License can be used, that proxy’s public statement of acceptance of a version permanently authorizes you to choose that version for the _Program_.

Later license versions may give you additional or different permissions. However, no additional obligations are imposed on any author or _Copyright_ holder as a result of your choosing to follow a later version.


### 15. Disclaimer of Warranty

THERE IS NO WARRANTY FOR THE PROGRAM, TO THE EXTENT PERMITTED BY APPLICABLE LAW. EXCEPT WHEN OTHERWISE STATED IN WRITING THE _COPYRIGHT_ HOLDERS AND/OR OTHER PARTIES PROVIDE THE PROGRAM “AS IS” WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE ENTIRE RISK AS TO THE QUALITY AND PERFORMANCE OF THE PROGRAM IS WITH YOU. SHOULD THE PROGRAM PROVE DEFECTIVE, YOU ASSUME THE COST OF ALL NECESSARY SERVICING, REPAIR OR CORRECTION.


### 16. Limitation of Liability

IN NO EVENT UNLESS REQUIRED BY APPLICABLE LAW OR AGREED TO IN WRITING WILL ANY _COPYRIGHT_ HOLDER, OR ANY OTHER PARTY WHO _MODIFIES_ AND/OR _CONVEYS_ THE PROGRAM AS PERMITTED ABOVE, BE LIABLE TO YOU FOR DAMAGES, INCLUDING ANY GENERAL, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OR INABILITY TO USE THE PROGRAM (INCLUDING BUT NOT LIMITED TO LOSS OF DATA OR DATA BEING RENDERED INACCURATE OR LOSSES SUSTAINED BY YOU OR THIRD PARTIES OR A FAILURE OF THE PROGRAM TO OPERATE WITH ANY OTHER PROGRAMS), EVEN IF SUCH HOLDER OR OTHER PARTY HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.


###  17. Interpretation of Sections 15 and 16

If the disclaimer of warranty and limitation of liability provided above cannot be given local legal effect according to their terms, reviewing courts shall apply local law that most closely approximates an absolute waiver of all civil liability in connection with the _Program_, unless a warranty or assumption of liability accompanies a copy of the _Program_ in return for a fee.

**END OF TERMS AND CONDITIONS**


## How to Apply These Terms to Your New Programs

If you develop a new program, and you want it to be of the greatest possible use to the public, the best way to achieve this is to make it free software which everyone can redistribute and change under these terms.

To do so, attach the following notices to the program. It is safest to attach them to the start of each source file to most effectively state the exclusion of warranty; and each file should have at least the “copyright” line and a pointer to where the full notice is found.

    <one line to give the program’s name and a brief idea of what it does.>
    Copyright (C) <year>  <name of author>

    This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.

Also add information on how to contact you by electronic and paper mail.

If the program does terminal interaction, make it output a short notice like this when it starts in an interactive mode:

    <program>  Copyright (C) <year>  <name of author>
    This program comes with ABSOLUTELY NO WARRANTY; for details type `show w’.
    This is free software, and you are welcome to redistribute it
    under certain conditions; type `show c’ for details.

The hypothetical commands “show w” and “show c” should show the appropriate parts of the General Public License. Of course, your program’s commands might be different; for a GUI interface, you would use an “about box”.

You should also get your employer (if you work as a programmer) or school, if any, to sign a “copyright disclaimer” for the program, if necessary. For more information on this, and how to apply and follow the GNU GPL, see <https://www.gnu.org/licenses/>.

The GNU General Public License does not permit incorporating your program into proprietary programs. If your program is a subroutine library, you may consider it more useful to permit linking proprietary applications with the library. If this is what you want to do, use the GNU Lesser General Public License instead of this License. But first, please read <https://www.gnu.org/licenses/why-not-lgpl.html>.

_________________________________________________________________

> TRADUCTION NON‑OFFICIELLE EN FRANÇAIS.  
> Par Lilian Tribouilloy et non approuvé par la FSF  
> **ATTENTION :** Cette traduction n’a pas de valeur juridique. Elle a simplement pour but d’aider à la compréhension des francophones.  
> **ATTENTION :** L’absence d’erreur de traduction ou l’absence de faute d’orthographe ne sont pas garantit.


# Licence Publique Générale GNU — Version 3, 29 juin 2007

_Tous Droits Réservés_ © 2007 Free Software Foundation, Inc. <https://fsf.org/>

Tout le monde est autorisé à copier et distribuer des copies textuelles de ce document de licence, mais sa modification n’est pas autorisée.


## Préambule

La licence publique générale GNU est une licence libre et un copyleft pour les logiciels et autres types d’œuvres.

Les licences pour la plupart des logiciels et autres travaux pratiques sont conçues pour _Vous_ priver de votre liberté de partager et de _Modifier_ les œuvres. En revanche, la licence publique générale GNU est destinée à garantir votre liberté de partager et de _Modifier_ toutes les versions d’un programme pour _Vous_ assurer qu’il reste un logiciel libre pour tous ses utilisateurs. Nous, la Free Software Foundation, utilisons la licence publique générale GNU pour la plupart de nos logiciels ; elle s’applique également à tout autre travail publié de cette manière par ses auteurs. _Vous_ pouvez également l’appliquer à _Vos_ programmes.

Lorsque nous parlons de logiciel libre, nous parlons de liberté, pas de prix. Nos licences publiques générales sont conçues pour _Vous_ assurer que _Vous_ avez la liberté de distribuer des copies de logiciels gratuits (et les facturer si _Vous_ le souhaitez), que _Vous_ pouvez recevoir le _Code Source_ ou l’obtenir si _Vous_ le souhaitez, que _Vous_ pouvez _Modifier_ le logiciel, ou que _Vous_ pouvez utiliser des morceaux de celui‑ci dans de nouveaux programmes libres, et que _Vous_ savez que _Vous_ pouvez faire toutes ces choses.

Pour protéger _Vos_ droits, nous devons empêcher les autres de _Vous_ refuser ces droits ou les empêcher de _Vous_ demander de renoncer à ces droits. Par conséquent, _Vous_ avez certaines responsabilités si _Vous_ distribuez des copies du logiciel, ou si _Vous_ le _Modifiez_ : des responsabilités de respecter la liberté d’autrui.

Par exemple, si _Vous_ distribuez des copies d’un tel programme, que ce soit à titre gratuit ou payant, _Vous_ devez _Transmettre_ aux destinataires les mêmes libertés que _Vous_ avez reçues. _Vous_ devez _Vous_ assurer qu’eux aussi reçoivent ou peuvent obtenir le _Code Source_. Et _Vous_ devez leur montrer ces conditions afin qu’ils connaissent leurs droits.

Les développeurs qui utilisent la GNU GPL protègent _Vos_ droits en deux étapes: **(1)** revendiquer le droit d’auteur sur le logiciel, et **(2)** _Vous_ offrir cette licence _Vous_ donnant l’autorisation légale de la copier, la distribuer et / ou la _Modifier_.

Pour la protection des développeurs et des auteurs, la GPL explique clairement qu’il n’y a aucune garantie pour ce logiciel libre. Pour le bien des utilisateurs et des auteurs, la GPL exige que les versions _Modifiées_ soient marquées comme _Modifiées_, afin que leurs problèmes ne soient pas attribués à tort aux auteurs des versions précédentes.

Certains appareils sont conçus pour refuser aux utilisateurs l’accès à l’installation ou à l’exécution de _Versions Modifiées_ du logiciel qu’ils contiennent, bien que le fabricant puisse le faire. Ceci est fondamentalement incompatible avec l’objectif de protéger la liberté des utilisateurs de _Modifier_ le logiciel. Le schéma systématique de ces abus se produit dans le domaine des produits destinés aux particuliers, qui est précisément là où il est le plus inacceptable. Par conséquent, nous avons conçu cette version de la GPL pour interdire cette pratique sur ces produits. Si de tels problèmes surviennent de manière substantielle dans d’autres domaines, nous sommes prêts à étendre cette disposition à ces domaines dans les futures versions de la GPL, si nécessaire pour protéger la liberté des utilisateurs.

Enfin, chaque programme est constamment menacé par les brevets logiciels. Les États ne devraient pas permettre aux brevets de restreindre le développement et l’utilisation de logiciels sur des ordinateurs à usage général, mais ils le font. Nous souhaitons éviter le danger particulier que les brevets, appliqués à un programme libre, puissent les transformer en un programme sous propriété industrielle. Pour éviter cela, la GPL garantit que les brevets ne peuvent pas être utilisés pour rendre _Le Programme_ non‑libre.

Voir les conditions précises de copie, de distribution et de _Modification_ suivantes.


## TERMES ET CONDITIONS


### 0. Définitions

« _Cette Licence_ » fait référence à la version 3 de la licence publique générale GNU.

« _Copyright_ » ou « _Tous Droits Réservés_ » signifie également les lois similaires au droit d’auteur qui s’appliquent à d’autres types d’œuvres, telles que les masques à semi-conducteurs.

« _Le Programme_ » fait référence à toute œuvre protégée par le droit d’auteur sous _Cette Licence_. Chaque titulaire de licence est appelé « _Vous_ ». Les « _Titulaires de Licence_ » et les « _Destinataires_ » peuvent être des individus ou des organisations. Soit des personnes juridiques.

« _Modifier_ » une œuvre signifie copier ou adapter tout ou partie de l’œuvre d’une manière nécessitant une autorisation du droit d’auteur, autre que la réalisation d’une copie exacte. Le travail qui en résulte est appelé une « _Version Modifiée_ » de l’œuvre antérieure ou une œuvre « _Basée Sur_ » l’œuvre antérieure.

Une « _Œuvre Couverte_ » désigne soit _Le Programme_ non _Modifié_, soit une œuvre _Basée Sur_ _Le Programme_.

« _Propager_ » une œuvre signifie faire quoi que ce soit qui, sans autorisation, _Vous_ rendrait directement ou indirectement responsable d’une violation en vertu de la loi sur le droit d’auteur applicable, sauf en l’exécutant sur un ordinateur ou en _Modifiant_ une copie privée. La _Propagation_ comprend la copie, la distribution (avec ou sans _Modification_), la mise à disposition du public et, dans certains pays, d’autres activités.

« _Transmettre_ » une œuvre signifie tout type de _Propagation_ qui permet à d’autres parties de faire ou de recevoir des copies. La simple interaction avec un utilisateur via un réseau informatique, sans transfert de copie, n’est pas une « _Transmission_ ».

Une interface utilisateur interactive affiche des « _Mentions Légales Appropriées_ » dans la mesure où elle inclut une fonction pratique et bien visible qui **(1)** affiche une notice de _Copyright_ appropriée, et **(2)** indique à l’utilisateur qu’il n’y a aucune garantie pour le travail (sauf pour la mesure dans laquelle les garanties sont fournies), que les titulaires de licence peuvent _Transmettre_ le travail sous _Cette Licence_, et comment visualiser une copie de _Cette Licence_. Si l’interface présente une liste de commandes ou d’options utilisateur, comme un menu, un élément important de la liste répond à ce critère.


### 1. Code Source

Le « _Code Source_ » d’une œuvre désigne la forme préférée de l’œuvre pour y apporter des _Modifications_. « _Code Objet_ » désigne toute forme non source d’une œuvre.

Une « _Interface Standard_ » désigne une interface qui est soit une norme officielle définie par un organisme de normalisation reconnu, soit, dans le cas des interfaces spécifiées pour un langage de programmation particulier, une interface largement utilisée par les développeurs travaillant dans ce langage.

Les « _Bibliothèques Système_ » d’un travail exécutable comprennent, autre que le travail dans son ensemble, tout ce qui **(a)** est inclus dans la forme normale d’empaquetage, un _Composant Majeur_, mais qui ne fait pas partie de ce _Composant Majeur_ ; et tout ce qui **(b)** sert uniquement à permettre l’utilisation du travail avec ce _Composant Majeur_, ou à implémenter une _Interface Standard_ pour laquelle une implémentation est disponible au public sous forme de _Code Source_. Un « _Composant Majeur_ », dans ce contexte, signifie un composant essentiel majeur (noyau, système de fenêtres, etc.) du système d’exploitation spécifique (le cas échéant) sur lequel s’exécute le travail exécutable, ou un compilateur utilisé pour produire le travail, ou un interpréteur de _Code Objet_ utilisé pour l’exécuter.

La « _Source Correspondante_ » pour un travail sous forme de _Code Objet_ signifie tout le _Code Source_ nécessaire pour générer, installer et (pour un travail exécutable) exécuter le _Code Objet_ et _Modifier_ le travail, y compris les scripts pour contrôler ces activités. Cependant, il n’inclut pas les _Bibliothèques Système_ de l’œuvre, ni les outils à usage général ou les programmes gratuits généralement disponibles qui sont utilisés non _Modifiés_ dans l’exécution de ces activités mais qui ne font pas partie du travail. Par exemple, la _Source Correspondante_ inclut les fichiers de définition d’interface associés aux fichiers source pour le travail, et le _Code Source_ pour les bibliothèques partagées et les sous-programmes liés dynamiquement que le travail est spécifiquement conçu pour exiger, comme par la communication de données intimes ou le flux de contrôle entre ces sous‑programmes et d’autres parties du travail.

La _Source Correspondante_ n’a pas besoin d’inclure quoi que ce soit que les utilisateurs peuvent régénérer automatiquement à partir d’autres parties de la _Source Correspondante_.

La _Source Correspondante_ pour un travail sous forme de _Code Source_ est ce même travail.


### 2. Autorisations de Base

Tous les droits accordés en vertu de _Cette Licence_ sont accordés pour la durée du droit d’auteur sur _Le Programme_ et sont irrévocables à condition que les conditions énoncées ici soient remplies. _Cette Licence_ affirme explicitement votre autorisation illimitée d’exécuter _Le Programme_ non _Modifié_. Le résultat de l’exécution d’une _Œuvre Couverte_ n’est couvert par _Cette Licence_ que si le résultat, compte tenu de son contenu, constitue une _Œuvre Couverte_. _Cette Licence_ reconnaît _Vos_ droits d’utilisation loyale ou autre équivalent, comme prévu par la loi sur les droits d’auteur.

_Vous_ pouvez créer, exécuter et _Propager_ des _Œuvres Couvertes_ que _Vous_ ne _Transmettez_ pas, sans conditions tant que votre licence reste par ailleurs en vigueur. _Vous_ pouvez _Transmettre_ des _Œuvres Couvertes_ à d’autres dans le seul but de leur faire apporter des _Modifications_ exclusivement pour _Vous_, ou _Vous_ fournir des installations pour exécuter ces œuvres, à condition que _Vous_ vous conformiez aux termes de _Cette Licence_ en _Transmettant_ tout le matériel pour lequel _Vous_ ne contrôlez pas le droits d’auteur. Ceux qui créent ou exploitent ainsi les _Œuvres Couvertes_ pour _Vous_ doivent le faire exclusivement en votre nom, sous votre direction et votre contrôle, dans des conditions qui leur interdisent de faire des copies de votre matériel protégé par le droit d’auteur en dehors de leur relation avec _Vous_.

La _Transmission_ dans toute autre circonstance est autorisé uniquement dans les conditions énoncées ci-dessous. La sous-licence n’est pas autorisée ; l’article **10** le rend inutile.


### 3. Protéger les Droits Légaux des Utilisateurs contre la Loi Anti‑Contournement

Aucune œuvre visée ne sera considérée comme faisant partie d’une mesure technique efficace en vertu d’une loi applicable remplissant les obligations découlant de l’[article 11](https://www.wipo.int/edocs/pubdocs/fr/wipo_pub_226.pdf) du traité de l’OMPI sur le droit d’auteur adopté le 20 décembre 1996 ou de lois similaires interdisant ou limitant le contournement de ces mesures.

Lorsque _Vous_ _Transmettez_ une _Œuvre Couverte_, _Vous_ renoncez à tout pouvoir légal d’interdire le contournement des mesures technologiques dans la mesure où ce contournement est effectué en exerçant des droits en vertu de la présente Licence en ce qui concerne l’_Œuvre Couverte_, et _Vous_ renoncez à toute intention de limiter l’exploitation ou la _Modification_ du travail comme un moyen de faire valoir, contre les utilisateurs de l’œuvre, _Vos_ droits légaux ou ceux de tiers d’interdire le contournement des mesures technologiques.


### 4. Transmission de Copies Textuelles

_Vous_ pouvez _Transmettre_ des copies textuelles du _Code Source du Programme_ au fur et à mesure que _Vous_ le recevez, sur n’importe quel support, à condition de publier de manière visible et appropriée sur chaque copie un avis de droit d’auteur approprié ; garder intactes tous les avis indiquant que _Cette Licence_ et toutes les conditions non permissives ajoutées conformément à l’article **7** s’appliquent au code ; garder intact tous les avis d’absence de toute garantie ; et remettez à tous les destinataires une copie de _Cette Licence_ avec _Le Programme_.

_Vous_ pouvez facturer n’importe quel prix ou aucun prix pour chaque copie que _Vous_ _Transmettez_, et _Vous_ pouvez offrir une assistance ou une protection sous garantie moyennant des frais.


### 5. Transmission des Versions Source Modifiées

_Vous_ pouvez _Transmettre_ une œuvre _Basée Sur_ _Le Programme_, ou les _Modifications_ pour la produire à partir du _Programme_, sous forme de _Code Source_ selon les termes de la section **4**, à condition que _Vous_ remplissiez également toutes ces conditions :

1. L’œuvre doit porter des avis bien visibles indiquant que _Vous_ l’avez _Modifiée_ et indiquant une date pertinente.
2. Le travail doit porter des avis bien visibles indiquant qu’il est publié sous _Cette Licence_ et toutes les conditions ajoutées à la section **7**. Cette exigence _Modifie_ l’exigence de la section **4** pour « garder intacts tous les avis ».
3. _Vous_ devez autoriser l’ensemble de l’œuvre, dans son ensemble, sous _Cette Licence_ à toute personne qui entre en possession d’une copie. _Cette Licence_ s’appliquera donc, avec toutes les conditions supplémentaires applicables de l’article **7**, à l’ensemble de l’œuvre et à toutes ses parties, quelle que soit la manière dont elles sont empaquetées. _Cette Licence_ ne donne aucune autorisation de concéder une licence sur l’œuvre d’une autre manière, mais elle n’invalide pas cette autorisation si _Vous_ l’avez reçue séparément.
4. Si l’œuvre a des interfaces utilisateurs interactives, chacune doit afficher des avis juridiques appropriés ; cependant, si _Le Programme_ dispose d’interfaces interactives qui n’affichent pas les mentions légales appropriées, votre travail n’a pas à les obliger à le faire.

Une compilation d’une _Œuvre Couverte_ avec d’autres œuvres séparées et indépendantes, qui ne sont pas par nature des extensions de l’_Œuvre Couverte_, et qui ne sont pas combinées avec celle-ci de manière à former un programme plus vaste, dans ou sur un volume de stockage ou d’une distribution support, est appelé un « _Agrégat_ ». À condition que la compilation et le droit d’auteur qui en résulte ne sont pas utilisés pour limiter l’accès ou les droits légaux des utilisateurs de la compilation au‑delà de ce que les œuvres individuelles le permettent. L’inclusion d’une _Œuvre Couverte_ dans un _Agrégat_ ne fait pas appliquer _Cette Licence_ aux autres parties de l’_Agrégat_.


### 6. Transmission des Éléments Non‑Code Sources

_Vous_ pouvez _Transmettre_ une _Œuvre Couverte_ sous forme de _Code Objet_ selon les termes des sections **4** et **5**, à condition que _Vous_ _Transmettiez_ également la _Source Correspondante_ lisible par machine selon les termes de _Cette Licence_, de l’une des manières suivantes :

1. _Transmettre_ le _Code Objet_ dans, ou incorporé dans, un produit physique (y compris un support de distribution physique), accompagné de la _Source Correspondante_ fixée sur un support physique durable habituellement utilisé pour l’échange de logiciels.
2. _Transmettre_ le _Code Objet_ dans, ou incorporé dans, un produit physique (y compris un support de distribution physique), accompagné d’une offre écrite, valable au moins trois ans et valable aussi longtemps que _Vous_ proposez des pièces de rechange ou un support client pour ce modèle de produit, pour donner à toute personne qui possède le _Code Objet_ soit **(1)** une copie de la _Source Correspondante_ pour tous les logiciels du _Produit Couverts_ par la présente Licence, sur un support physique durable habituellement utilisé pour l’échange de logiciels, pour un prix non plus que votre coût raisonnable pour effectuer physiquement ce transfert de source, ou **(2)** l’accès pour copier gratuitement la _Source Correspondante_ à partir d’un serveur réseau.
3. _Transmettre_ des copies individuelles du _Code Objet_ avec une copie de l’offre écrite pour fournir la _Source Correspondante_. Cette alternative n’est autorisée qu’occasionnellement et à des fins non commerciales, et uniquement si _Vous_ avez reçu le _Code Objet_ avec une telle offre, conformément au paragraphe **6.2**.
4. _Transmettre_ le _Code Objet_ en offrant un accès depuis un endroit désigné (gratuitement ou moyennant un supplément), et offrir un accès équivalent à la _Source Correspondante_ de la même manière via le même endroit sans frais supplémentaires. _Vous_ n’avez pas besoin d’exiger des destinataires qu’ils copient la _Source Correspondante_ avec le _Code Objet_. Si le lieu de copie du _Code Objet_ est un serveur réseau, la _Source Correspondante_ peut se trouver sur un serveur différent (géré par _Vous_ ou par un tiers) qui prend en charge des fonctions de copie équivalentes, à condition que _Vous_ mainteniez des instructions claires à côté du _Code Objet_ indiquant où trouver la _Source Correspondante_. Quel que soit le serveur hébergeant la _Source Correspondante_, _Vous_ restez obligé de _Vous_ assurer qu’elle est disponible aussi longtemps que nécessaire pour satisfaire ces exigences.
5. _Transmettre_ le _Code Objet_ en utilisant la _Transmission_ peer-to-peer, à condition que _Vous_ informiez d’autres pairs où le _Code Objet_ et la _Source Correspondante_ de l’œuvre sont offerts au grand public sans frais en vertu du paragraphe 6d.

Une partie séparable du _Code Objet_, dont le _Code Source_ est exclu de la _Source Correspondante_ en tant que _Bibliothèque Système_, n’a pas besoin d’être incluse dans la _Transmission_ du travail de _Code Objet_.

Un « _Produit Utilisateur_ » est soit **(1)** un « _Produit de Consommation_ », qui signifie tout bien meuble corporel qui est _Normalement Utilisé_ à des fins personnelles, familiales ou domestiques, ou **(2)** tout ce qui est conçu ou vendu pour être incorporé dans un logement. Pour déterminer si un produit est un _Produit de Consommation_, les cas douteux doivent être résolus en faveur de la couverture. Pour un produit particulier reçu par un utilisateur particulier, « _Normalement Utilisé_ » fait référence à une utilisation typique ou courante de cette classe de produits, quel que soit le statut de l’utilisateur particulier ou la manière dont l’utilisateur particulier utilise réellement, ou attend ou devrait utiliser, le produit. Un produit est un _Produit de Consommation_ indépendamment du fait que le produit ait des utilisations commerciales, industrielles ou non-consommateurs substantielles, à moins que ces utilisations ne représentent le seul mode d’utilisation significatif du produit.

« _Informations d’Installation_ » pour un _Produit Utilisateur_ désigne toutes les méthodes, procédures, clés d’autorisation ou autres informations requises pour installer et exécuter des _Versions Modifiées_ d’une _Œuvre Couverte_ dans ce _Produit Utilisateur_ à partir d’une _Version Modifiée_ de sa _Source Correspondante_. Les informations doivent suffire à garantir que le fonctionnement continu du _Code Objet_ _Modifié_ ne soit en aucun cas empêché ou perturbé du seul fait qu’une _Modification_ a été apportée.

Si _Vous_ _Transmettez_ un _Code Objet_, travaillez dans le cadre de cette section dans, ou avec, ou spécifiquement pour une utilisation dans, un _Produit Utilisateur_, et le transfert se produit dans le cadre d’une transaction dans laquelle le droit de possession et d’utilisation du _Produit Utilisateur_ est transféré au destinataire à perpétuité ou pour une durée déterminée (quelle que soit la manière dont la transaction est caractérisée), la _Source Correspondante_ _Transmise_ dans le cadre de cette section doit être accompagnée des _Informations d’Installation_. Mais cette exigence ne s’applique pas si ni _Vous_ ni aucun tiers ne conserve la possibilité d’installer le _Code Objet_ _Modifié_ sur le _Produit Utilisateur_ (par exemple, le travail a été installé dans la ROM).

L’obligation de fournir des _Informations d’Installation_ n’inclut pas l’obligation de continuer à fournir un service d’assistance, une garantie ou des mises à jour pour un travail qui a été _Modifié_ ou installé par le destinataire, ou pour le _Produit Utilisateur_ dans lequel il a été _Modifié_ ou installé. L’accès à un réseau peut être refusé lorsque la _Modification_ elle-même affecte matériellement et négativement le fonctionnement du réseau ou enfreint les règles et protocoles de communication à travers le réseau.

La _Source Correspondante_ _Transmise_ et les _Informations d’Installation_ fournies, conformément à cette section, doivent être dans un format documenté publiquement (et avec une implémentation accessible au public sous forme de _Code Source_), et ne doivent nécessiter aucun mot de passe ou clé spécial pour le déballage, la lecture ou la copie.


### 7. Conditions Additionelles

Les « _Autorisations Supplémentaires_ » sont des termes qui complètent les termes de _Cette Licence_ en faisant des exceptions à une ou plusieurs de ses conditions. Les _Autorisations Supplémentaires_ applicables à l’ensemble du Programme seront traitées comme si elles étaient incluses dans _Cette Licence_, dans la mesure où elles sont valables en vertu de la loi applicable. Si des _Autorisations Supplémentaires_ ne s’appliquent qu’à une partie du programme, cette partie peut être utilisée séparément sous ces autorisations, mais l’ensemble du programme reste régi par _Cette Licence_ sans égard aux _Autorisations Supplémentaires_.

Lorsque _Vous_ _Transmettez_ une copie d’une _Œuvre Couverte_, _Vous_ pouvez, à votre choix, supprimer toute autorisation supplémentaire de cette copie ou de toute partie de celle-ci. (Des _Autorisations Supplémentaires_ peuvent être écrites pour exiger leur propre suppression dans certains cas lorsque _Vous_ _Modifiez_ l’œuvre.) _Vous_ pouvez placer des _Autorisations Supplémentaires_ sur le matériel que _Vous_ avez ajouté à une _Œuvre Couverte_, pour lequel _Vous_ avez ou pouvez donner l’autorisation de droit d’auteur appropriée.

Nonobstant toute autre disposition de _Cette Licence_, pour le matériel que _Vous_ ajoutez à une _Œuvre Couverte_, _Vous_ pouvez (si autorisé par les détenteurs des droits d’auteur de ce matériel) compléter les termes de _Cette Licence_ avec des termes:

1. Refuser la garantie ou limiter la responsabilité différemment des termes des articles **15** et **16** de _Cette Licence_ ; ou
2. Exiger la préservation des mentions légales raisonnables spécifiées ou des attributions d’auteur dans ce matériel ou dans les mentions légales appropriées affichées par les œuvres qui le contiennent ; ou
3. Interdire la fausse représentation de l’origine de ce matériel, ou exiger que les _Versions Modifiées_ de ce matériel soient marquées de manière raisonnable comme étant différentes de la version originale ; ou
4. Limiter l’utilisation à des fins publicitaires des noms des concédants de licence ou des auteurs du matériel ; ou
5. Refuser d’accorder des droits en vertu du droit des marques pour l’utilisation de certains noms commerciaux, marques ou marques de service ; ou
6. Exiger une indemnisation des concédants de licence et des auteurs de ce matériel par toute personne qui _Transmet_ le matériel (ou des _Versions Modifiées_ de celui-ci) avec des hypothèses contractuelles de responsabilité envers le destinataire, pour toute responsabilité que ces hypothèses contractuelles imposent directement à ces concédants et auteurs.

Toutes les autres conditions supplémentaires non permissives sont considérées comme des « restrictions supplémentaires » au sens de la section **10**. Si _Le Programme_ tel que _Vous_ l’avez reçu, ou une partie de celui-ci, contient un avis indiquant qu’il est régi par _Cette Licence_ avec un terme qui est une restriction supplémentaire, _Vous_ pouvez supprimer ce terme. Si un document de licence contient une restriction supplémentaire mais autorise une nouvelle licence ou un transfert en vertu de _Cette Licence_, _Vous_ pouvez ajouter à un travail couvert régi par les termes de ce document de licence, à condition que la restriction supplémentaire ne survit pas à cette nouvelle licence ou à ce transfert.

Si _Vous_ ajoutez des termes à une _Œuvre Couverte_ conformément à cette section, _Vous_ devez placer, dans les fichiers source concernés, un énoncé des conditions supplémentaires qui s’appliquent à ces fichiers, ou un avis indiquant où trouver les termes applicables.

Des conditions supplémentaires, permissives ou non permissives, peuvent être énoncées sous la forme d’une licence écrite séparément, ou indiquées comme exceptions ; les exigences ci-dessus s’appliquent dans les deux cas.


### 8. Violation des Termes

_Vous_ ne pouvez pas _Propager_ ou _Modifier_ une _Œuvre Couverte_ sauf dans les cas expressément prévus par _Cette Licence_. Toute tentative de _Propager_ ou de _Modifier_ autrement est nulle et mettra automatiquement fin à _Vos_ droits en vertu de _Cette Licence_ (y compris toute _Licence de Brevet_ accordée en vertu du troisième paragraphe de la section **11**).

Cependant, si _Vous_ cessez toute violation de _Cette Licence_, alors votre licence d’un détenteur de droits d’auteur particulier est rétablie **(a)** provisoirement, à moins et jusqu’à ce que le détenteur des droits d’auteur résilie explicitement et définitivement votre licence, et **(b)** définitivement, si le détenteur des droits d’auteur échoue. pour _Vous_ informer de la violation par des moyens raisonnables avant 60 jours après la cessation.

De plus, votre licence d’un détenteur de droits d’auteur particulier est rétablie de manière permanente si le détenteur des droits d’auteur _Vous_ informe de la violation par des moyens raisonnables, c’est la première fois que _Vous_ recevez un avis de violation de _Cette Licence_ (pour tout travail) de ce détenteur de droits d’auteur, et _Vous_ remédiez à la violation dans les 30 jours suivant la réception de l’avis.

La résiliation de _Vos_ droits en vertu de cette section ne met pas fin aux licences des parties qui ont reçu des copies ou des droits de votre part en vertu de _Cette Licence_. Si _Vos_ droits ont été résiliés et n’ont pas été définitivement rétablis, _Vous_ n’êtes pas admissible à recevoir de nouvelles licences pour le même matériel en vertu de l’article 10.


### 9. Acceptation non Requise pour Avoir des Copies

_Vous_ n’êtes pas obligé d’accepter _Cette Licence_ pour recevoir ou exécuter une copie du programme. La _Propagation_ auxiliaire d’une _Œuvre Couverte_ se produisant uniquement à la suite de l’utilisation de la _Transmission_ peer-to-peer pour recevoir une copie ne nécessite pas non plus d’acceptation. Cependant, rien d’autre que _Cette Licence_ ne _Vous_ accorde la permission de _Propager_ ou de _Modifier_ tout travail couvert. Ces actions enfreignent le droit d’auteur si _Vous_ n’acceptez pas _Cette Licence_. Par conséquent, en _Modifiant_ ou en _Propageant_ une _Œuvre Couverte_, _Vous_ indiquez que _Vous_ acceptez _Cette Licence_ pour le faire.


### 10. Licence Automatique des Destinataires en Aval

Chaque fois que _Vous_ _Transmettez_ un travail couvert, le destinataire reçoit automatiquement une licence des concédants d’origine, pour exécuter, _Modifier_ et _Propager_ ce travail, sous réserve de _Cette Licence_. _Vous_ n’êtes pas responsable de l’application du respect par des tiers de _Cette Licence_.

Une « _Transaction d’Entité_ » est une transaction transférant le contrôle d’une organisation, ou de la quasi-totalité des actifs d’une organisation, ou subdivisant une organisation, ou fusionnant des organisations. Si la _Propagation_ d’une _Œuvre Couverte_ résulte d’une _Transaction d’Entité_, chaque partie à cette transaction qui reçoit une copie de l’œuvre reçoit également toutes les licences sur l’œuvre que le prédécesseur de la partie intéressée avait ou pourrait donner en vertu du paragraphe précédent, plus un droit de possession. de la _Source Correspondante_ de l’œuvre du prédécesseur concerné, si le prédécesseur l’a ou peut l’obtenir avec des efforts raisonnables.

_Vous_ ne pouvez imposer aucune autre restriction à l’exercice des droits accordés ou confirmés en vertu de _Cette Licence_. Par exemple, _Vous_ ne pouvez pas imposer de frais de licence, de redevance ou d’autres frais pour l’exercice des droits accordés en vertu de _Cette Licence_, et _Vous_ ne pouvez pas engager de litige (y compris une réclamation croisée ou une demande reconventionnelle dans un procès) alléguant qu’une revendication de brevet est violée. en créant, en utilisant, en vendant, en offrant à la vente ou en important _Le Programme_ ou une partie de celui‑ci.


### 11. Brevets

Un « _Contributeur_ » est un détenteur de droits d’auteur qui autorise l’utilisation sous _Cette Licence_ du Programme ou d’une œuvre sur laquelle _Le Programme_ est basé. L’œuvre ainsi concédée est appelée « _Version Contributrice_ » du _Contributeur_.

Les « _Revendications de Brevet Essentielles_ » d’un _Contributeur_ sont toutes les revendications de brevet détenues ou contrôlées par le _Contributeur_, qu’elles soient déjà acquises ou acquises par la suite, qui seraient enfreintes d’une manière quelconque, permise par la présente Licence, en créant, en utilisant ou en vendant sa version de _Contributeur_, mais n’incluez pas les réclamations qui seraient violées uniquement à la suite d’une _Modification_ ultérieure de la version du _Contributeur_. Aux fins de cette définition, le « _contrôle_ » comprend le droit d’accorder des sous-licences de brevet d’une manière compatible avec les exigences de _Cette Licence_.

Chaque _Contributeur_ _Vous_ accorde une _Licence de Brevet_ non exclusive, mondiale et libre de redevance en vertu des _Revendications de Brevet Essentielles_ du _Contributeur_, pour créer, utiliser, vendre, proposer à la vente, importer et exécuter, _Modifier_ et _Propager_ le contenu de sa version _Contributeur_.

Dans les trois paragraphes suivants, une « _Licence de Brevet_ » est tout accord ou engagement exprès, quelle qu’en soit la dénomination, de ne pas appliquer un brevet (comme une autorisation expresse de pratiquer un brevet ou un engagement de ne pas intenter de poursuites pour contrefaçon de brevet). «Accorder» une telle _Licence de Brevet_ à une partie signifie conclure un tel accord ou s’engager à ne pas faire valoir un brevet contre la partie.

Si _Vous_ _Transmettez_ une _Œuvre Couverte_, en _Vous_ appuyant sciemment sur une _Licence de Brevet_, et que la _Source Correspondante_ de l’œuvre n’est pas disponible pour quiconque puisse la copier, gratuitement et selon les termes de _Cette Licence_, via un serveur de réseau accessible au public ou autre facilement accessible signifie, alors _Vous_ devez soit **(1)** faire en sorte que la _Source Correspondante_ soit ainsi disponible, soit **(2)** prendre des dispositions pour _Vous_ priver du bénéfice de la _Licence de Brevet_ pour cette œuvre particulière, ou **(3)** organiser, d’une manière compatible avec le exigences de _Cette Licence_, pour étendre la _Licence de Brevet_ aux destinataires en aval. «Se fier sciemment» signifie que _Vous_ savez réellement que, sans la _Licence de Brevet_, le fait de _Transmettre_ l’_Œuvre Couverte_ dans un pays ou l’utilisation par votre destinataire de l’_Œuvre Couverte_ dans un pays enfreindrait un ou plusieurs brevets identifiables dans ce pays que _Vous_ avoir des raisons de croire sont valides.

Si, conformément à ou en relation avec une transaction ou un arrangement unique, _Vous_ _Transmettez_, ou _Propagez_ en procurant le transfert d’une _Œuvre Couverte_, et accordez une _Licence de Brevet_ à certaines des parties recevant l’_Œuvre Couverte_, les autorisant à utiliser, _Propager_, _Modifier_ ou _Transmettre_ une copie spécifique de l’_Œuvre Couverte_, la _Licence de Brevet_ que _Vous_ accordez est automatiquement étendue à tous les destinataires de l’_Œuvre Couverte_ et des œuvres _Basées Sur_ celle-ci.

Une _Licence de Brevet_ est « _Discriminatoire_ » si elle n’inclut pas dans le champ d’application de sa couverture, interdit l’exercice ou est conditionnée au non-exercice d’un ou plusieurs des droits qui sont spécifiquement accordés en vertu de _Cette Licence_. _Vous_ ne pouvez pas _Transmettre_ une _Œuvre Couverte_ si _Vous_ êtes partie à un accord avec un tiers qui est dans le secteur de la distribution de logiciels, en vertu duquel _Vous_ effectuez un paiement au tiers en fonction de l’étendue de votre activité de _Transmission_ de l’œuvre, et en vertu de laquelle le tiers accorde, à l’une des parties qui recevrait l’_Œuvre Couverte_ de votre part, une _Licence de Brevet_ _Discriminatoire_ **(a)** en relation avec des copies de l’_Œuvre Couverte_ que _Vous_ avez _Transmises_ (ou des copies faites à partir de ces copies), ou **(b)** principalement pour et en relation avec des produits ou des compilations spécifiques contenant l’_Œuvre Couverte_, à moins que _Vous_ n’ayez conclu cet accord, ou que _Cette Licence_ de brevet n’ait été accordée, avant le 28 mars 2007.

Rien dans _Cette Licence_ ne doit être interprété comme excluant ou limitant toute licence implicite ou autres moyens de défense à la contrefaçon qui pourraient autrement _Vous_ être disponibles en vertu du droit des brevets applicable.


### 12. Pas d’Abandon de la Liberté d’Autrui

Si des conditions _Vous_ sont imposées (que ce soit par décision judiciaire, accord ou autre) qui contredisent les conditions de _Cette Licence_, elles ne _Vous_ dispensent pas des conditions de _Cette Licence_. Si _Vous_ ne pouvez pas _Transmettre_ une _Œuvre Couverte_ de manière à satisfaire simultanément _Vos_ obligations en vertu de _Cette Licence_ et toute autre obligation pertinente, _Vous_ ne pouvez par conséquent pas la _Transmettre_ du tout. Par exemple, si _Vous_ acceptez des conditions qui _Vous_ obligent à percevoir une redevance pour la _Transmission_ ultérieure de ceux à qui _Vous_ _Transmettez_ _Le Programme_, la seule façon de satisfaire ces conditions et _Cette Licence_ serait de _Vous_ abstenir entièrement de _Transmettre_ _Le Programme_.


### 13. Utilisation avec la Licence Publique Générale GNU Affero

Nonobstant toute autre disposition de _Cette Licence_, _Vous_ avez la permission de lier ou de combiner tout travail couvert avec un travail sous licence sous la version 3 de la Licence Publique Générale GNU Affero en un seul travail combiné, et de _Transmettre_ le travail résultant. Les termes de _Cette Licence_ continueront de s’appliquer à la partie qui est l’_Œuvre Couverte_, mais les exigences spéciales de la licence publique générale GNU Affero, section 13, concernant l’interaction via un réseau s’appliqueront à la combinaison en tant que telle.


### 14. Versions Révisées de cette Licence

La Free Software Foundation peut publier de temps à autre des versions révisées et / ou nouvelles de la licence publique générale GNU. Ces nouvelles versions seront similaires dans l’esprit à la version actuelle, mais peuvent différer en détail pour répondre à de nouveaux problèmes ou préoccupations.

Un numéro distinct est attribué à chaque version. Si _Le Programme_ spécifie qu’une certaine version numérotée de la Licence Publique Générale GNU « ou toute version ultérieure » s’applique à lui, _Vous_ avez la possibilité de suivre les termes et conditions de cette version numérotée ou de toute version ultérieure publiée par le Logiciel Libre Fondation. Si _Le Programme_ ne spécifie pas de numéro de version de la Licence Publique Générale GNU, _Vous_ pouvez choisir n’importe quelle version publiée par la Free Software Foundation.

Si _Le Programme_ spécifie qu’un mandataire peut décider quelles futures versions de la licence publique générale GNU peuvent être utilisées, la déclaration publique d’acceptation d’une version de ce mandataire _Vous_ autorise en permanence à choisir cette version pour _Le Programme_.

Les versions de licence ultérieures peuvent _Vous_ donner des _Autorisations Supplémentaires_ ou différentes. Cependant, aucune obligation supplémentaire n’est imposée à tout auteur ou détenteur de droits d’auteur en raison de votre choix de suivre une version ultérieure.


### 15. Exclusion de Garantie

IL N’Y A AUCUNE GARANTIE POUR _LE PROGRAMME_, DANS LA MESURE AUTORISÉE PAR LA LOI APPLICABLE. Sauf indication contraire dans un écrit, les titulaires du droit d’auteur et / ou d’autres parties fournissent _Le Programme_ « tel quel » sans aucune garantie de quelque nature que ce soit, explicite ou implicite, y compris, mais sans s’y limiter, les garanties implicites de qualité marchande et d’adéquation à une partie . TOUT RISQUE LIÉ À LA QUALITÉ ET À LA PERFORMANCE DU PROGRAMME EST AVEC _Vous_. SI _LE PROGRAMME_ S’AVÈRE DÉFECTUEUX, _Vous_ ASSUMEZ LE COÛT DE TOUS LES ENTRETIENS, RÉPARATIONS OU CORRECTIONS NÉCESSAIRES.


### 16. Limitation de Responsabilité

EN AUCUN CAS, SAUF EXIGÉ PAR LA LOI APPLICABLE OU ACCEPTÉ PAR ÉCRIT, TOUT TITULAIRE DU _COPYRIGHT_, OU TOUT AUTRE PARTIE QUI _MODIFIE_ ET / OU CONVOYE _Le Programme_ COMME AUTORISÉ CI-DESSUS, NE SERA RESPONSABLE DE _Vous_ POUR LES DOMMAGES, Y COMPRIS TOUT DÉTENTE GÉNÉRAL, SPÉCIAL, ACCESSOIRE OU INDIRECT DOMMAGES RÉSULTANT DE L’UTILISATION OU DE L’INCAPACITÉ D’UTILISER _LE PROGRAMME_ (Y COMPRIS MAIS SANS S’Y LIMITER LA PERTE DE DONNÉES OU DES DONNÉES RENDUES INEXACTES OU DES PERTES SOUTENUES PAR _Vous_ OU DES TIERS OU UN ÉCHEC DU PROGRAMME DE FONCTIONNER AVEC TOUT AUTRE PROGRAMME), MÊME SI CE TITULAIRE OU AUTRE PARTIE A ÉTÉ AVISÉ DE LA POSSIBILITÉ DE TELS DOMMAGES.


### 17. Interprétation des Sections 15 et 16

Si la dénégation de garantie et la limitation de responsabilité ci-dessus ne peuvent pas avoir d’effet juridique local selon leurs conditions, les tribunaux de révision appliqueront la loi locale qui se rapproche le plus d’une renonciation absolue à toute responsabilité civile en relation avec _Le Programme_, à moins d’une garantie ou d’une hypothèse. de responsabilité accompagne une copie du programme en échange de frais.

**FIN DES TERMES ET CONDITIONS**


## Comment Appliquer ces Conditions à vos Nouveaux Programmes

Si _Vous_ développez un nouveau programme et que _Vous_ voulez qu’il soit de la plus grande utilité possible pour le public, la meilleure façon d’y parvenir est d’en faire un logiciel libre que tout le monde peut redistribuer et _Modifier_ dans ces conditions.

Pour ce faire, joignez les avis suivants au programme. Il est plus sûr de les joindre au début de chaque fichier source pour indiquer le plus efficacement possible l’exclusion de garantie ; et chaque fichier doit avoir au moins la ligne «_Copyright_» et un pointeur vers l’emplacement de l’avis complet.

     <une ligne pour donner le nom du programme et une brève idée de ce qu’il fait.>
     Copyright (C) <année> <nom de l’auteur>

     Ce programme est un logiciel libre: _Vous_ pouvez le redistribuer et / ou le modifier
     sous les termes de la licence publique générale GNU publiée par
     la Free Software Foundation, soit la version 3 de la licence, soit
     (à votre choix) toute version ultérieure.

     Ce programme est distribué dans l’espoir qu’il sera utile,
     mais SANS AUCUNE GARANTIE ; sans même la garantie implicite de
     QUALITÉ MARCHANDE ou ADAPTATION À UN USAGE PARTICULIER. Voir le
     GNU General Public License pour plus de détails.

     _Vous_ devriez avoir reçu une copie de la licence publique générale GNU
     avec ce programme. Sinon, consultez <https://www.gnu.org/licenses/>.

Ajoutez également des informations sur la manière de _Vous_ contacter par courrier électronique et papier.

Si _Le Programme_ interagit avec le terminal, faites-lui sortir un court avis comme celui-ci lorsqu’il démarre en mode interactif:

     <programme> Copyright (C) <année> <nom de l’auteur>
     Ce programme est livré avec ABSOLUMENT AUCUNE GARANTIE ; pour plus de détails, tapez «show w».
     Il s’agit d’un logiciel gratuit et _Vous_ êtes invité à le redistribuer
     sous certaines conditions ; tapez «show c» pour plus de détails.

Les commandes hypothétiques «show w» et «show c» devraient afficher les parties appropriées de la licence publique générale. Bien sûr, les commandes de votre programme peuvent être différentes; pour une interface GUI, _Vous_ utiliseriez une «boîte à propos».

_Vous_ devriez également demander à votre employeur (si _Vous_ travaillez en tant que programmeur) ou à votre école, le cas échéant, de signer une « clause de non-responsabilité de droits d’auteur » pour _Le Programme_, si nécessaire. Pour plus d’informations à ce sujet, et comment appliquer et suivre la GNU GPL, consultez <https://www.gnu.org/licenses/>.

La licence publique générale GNU ne permet pas d’incorporer votre programme dans des programmes propriétaires. Si votre programme est une bibliothèque de sous-programmes, _Vous_ pouvez considérer qu’il est plus utile d’autoriser la liaison des applications propriétaires avec la bibliothèque. Si c’est ce que _Vous_ voulez faire, utilisez la Licence Publique Générale Limitée GNU au lieu de _Cette Licence_. Mais d’abord, veuillez lire <https://www.gnu.org/licenses/why-not-lgpl.html>.

