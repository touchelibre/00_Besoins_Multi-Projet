**In English:**

{Product Name} is {description}.  
{Product Name} is a device of ToucheLibre project.

The aim of ToucheLibre project is to design ergonomic human / machine interfaces with following values: beautiful aesthetic, respect of health user, fight against planned obsolescence and ecologic responsability.

Copyright © Lilian Tribouilloy 2020

All the electronical or mechanical part of this project (all the source files in this directory) are under the **CERN Open Hardware Licence - Strongly Reciprocal version 2 or later version**.

In according to the SPDX specification from <https://spdx.org/licenses/>, the identifiers are:  
- SPDX-FileCopyrightText: 2020 Lilian Tribouilloy <lilian@touchelibre.fr>
- SPDX-License-Identifier: CERN-OHL-S-2.0+

You may redistribute and modify this source and make products using it under the terms of the CERN‑OHL‑S v2 or later version.

The original text of this licence is the CERN’s repository : <https://ohwr.org/cern_ohl_s_v2.txt>

This source is distributed WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY, SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. Please see the CERN‑OHL‑S v2 or later version for applicable conditions.

Source locationof ToucheLibre project:  
<http://touchelibre.fr> and <https://gitlab.com/touchelibre>

As per CERN‑OHL‑S v2 section 4, should You produce hardware based on this source, You must where practicable maintain the Source Location visible on the external case of the {Product Name} or other products you make using this source.

______________________________________________________________

**En Français :**

{Product Name} est {description}.
{Product Name} est un appareil du projet ToucheLibre.

L'objectif du projet ToucheLibre est de concevoir des interfaces homme / machine ergonomiques avec les valeurs suivantes : belle esthétique, respect de la santé de l'utilisateur, lutte contre l'obsolescence programmée et responsabilité écologique.

Tous droits réservés © Lilian Tribouilloy 2020

Toutes les parties électronique et mécanique de ce projet (tous les fichiers sources présents dans ce répertoire) sont placé sous **licence CERN Open Hardware − Fortement Réciproque version 2 ou toute version ultérieure**.

En accord avec la spécification SPDX depuis <https://spdx.org/licenses/>, les identifiants sont :  
- SPDX-FileCopyrightText: 2020 Lilian Tribouilloy <lilian@touchelibre.fr>
- SPDX-License-Identifier: CERN-OHL-S-2.0+

Vous pouvez redistribuer et modifier cette source et fabriquer des produits en l’utilisant sous les termes du CERN‑OHL‑S v2 ou toute version ultérieure.

Le texte originale de cette licence se trouve dans le repository du CERN : <https://ohwr.org/cern_ohl_s_v2.txt>. Pour votre compréhension, une traduction non‑officielle en français, mais sans valeur juridique, est également proposé à la fin de ce fichier.

Cette source est distribuée SANS AUCUNE GARANTIE EXPRESSE OU IMPLICITE, Y COMPRIS LA QUALITÉ MARCHANDE, LA QUALITÉ SATISFAISANTE ET L’ADÉQUATION POUR UNE RAISON PARTICULIÈRE. Veuillez consulter le CERN‑OHL‑S v2 ou toute version ultérieure pour les conditions applicables.

Emplacement des sources du projet ToucheLibre :  
<http://touchelibre.fr> et <https://gitlab.com/touchelibre>

Conformément à la section 4 du CERN‑OHL‑S v2, si vous produisez du matériel sur cette base source, Vous devez, dans la mesure du possible, maintenir l’emplacement de la source visible sur le boîtier externe du {Nom du Produit} ou d’autres produits que vous fabriquez en utilisant cette source.


______________________________________________________________

> You can find bellow a copy of this licence just for your information. And in according to the requirements from the User Guide, <https://ohwr.org/project/cernohl/wikis/uploads/b88fd806c337866bff655f2506f23d37/cern_ohl_s_v2_user_guide.txt>  
>   
> This copy have some esthetical modifications so only original text in CERN’s repository is applicable.  
>  
> These esthetical modifications are :  
> * Translation in markdown format,  
> * Words in definition (§1) are in emphasis in all the text,  
> * The different elements, between Strongly version and Weakly version of CERN-OW licence, are in strong emphasis,  
> * Some modifications to improve the typography,  
> * Some modifications on the subsection numbers to get better view on the differences between CERN-OWL-S and CERN-OWL-W.


# CERN Open Hardware Licence Version 2 − Strongly Reciprocal


## Preamble

CERN has developed this licence to promote collaboration among hardware designers and to provide a legal tool which supports the freedom to use, study, modify, share and distribute hardware designs and products based on those designs. Version 2 of the CERN Open Hardware Licence comes in three variants: CERN‑OHL‑P (permissive); and two reciprocal licences: CERN‑OHL‑W (weakly reciprocal) and this licence, CERN‑OHL‑S (strongly reciprocal).

The CERN‑OHL‑S is copyright © CERN 2020. Anyone is welcome to use it, in unmodified form only.

Use of this _Licence_ does not imply any endorsement by CERN of any _Licensor_ or their designs nor does it imply any involvement by CERN in their development.


## 1. Definitions

**1.1.** ‘_Licence_’ means this CERN‑OHL‑**S**.

**1.2.** ‘_Compatible Licence_’ means
1. any earlier version of the CERN Open Hardware licence, or
2. any version of the CERN‑OHL‑S, or
3. any licence which permits _You_ to treat the _Source_ to which it applies as licensed under CERN‑OHL‑S provided that on _Conveyance_ of any such _Source_, or any associated _Product_ _You_ treat the _Source_ in question as being licensed under CERN‑OHL‑S.

**1.3.** ‘_Source_’ means information such as design materials or digital code which can be applied to _Make_ or test a _Product_ or to prepare a _Product_ for use, _Conveyance_ or sale, regardless of its medium or how it is expressed. It may include _Notices_.

**1.4.** ‘_Covered Source_’ means _Source_ that is explicitly made available under this _Licence_.

**1.5.** ‘_Product_’ means any device, component, work or physical object, whether in finished or intermediate form, arising from the use, application or processing of _Covered Source_.

**1.6.** ‘_Make_’ means to create or configure something, whether by manufacture, assembly, compiling, loading or applying _Covered Source_ or another _Product_ or otherwise.

**1.7.** ‘_Available Component_’ means any part, sub-assembly, library or code which:
1. is licensed to _You_ as _Complete Source_ under a _Compatible Licence_; or
2. is available, at the time a _Product_ or the _Source_ containing it is first _Conveyed_, to _You_ and any other prospective licensees:
	- **as a physical part** with sufficient rights and information (including any configuration and programming files and information about its characteristics and interfaces) to enable it either to be _Made_ itself, or to be sourced and used to _Make_ the _Product_; or
	- **as part of the normal distribution** of a tool used to design or _Make_ the _Product_.

**1.8.** ‘_Complete Source_’ means the set of all _Source_ necessary to _Make_ a _Product_, in the preferred form for making modifications, including necessary installation and interfacing information both for the _Product_, and for any included _Available Components_. If the format is proprietary, it must also be made available in a format (if the proprietary tool can create it) which is viewable with a tool available to potential licensees and licensed under a licence approved by the Free Software Foundation (<https://www.fsf.org/>) or the Open Source Initiative (<https://opensource.org/>). _Complete Source_ need not include the _Source_ of any _Available Component_, provided that _You_ include in the _Complete Source_ sufficient information to enable a recipient to _Make_ or source and use the _Available Component_ to _Make_ the _Product_.

**1.9.** ‘_Source Location_’ means a location where a _Licensor_ has placed _Covered Source_, and which that _Licensor_ reasonably believes will remain easily accessible for at least three years for anyone to obtain a digital copy.

**1.10.** ‘_Notice_’ means copyright, acknowledgement and trademark notices, _Source Location_ references, modification notices (subsection **3.3(2)**) and all notices that refer to this _Licence_ and to the disclaimer of warranties that are included in the _Covered Source_.

**1.11.** ‘_Licensee_’ or ‘_You_’ means any person exercising rights under this _Licence_.

**1.12.** ‘_Licensor_’ means a natural or legal person who creates or modifies _Covered Source_. A person may be a _Licensee_ and a _Licensor_ at the same time.

**1.13.** ‘_Convey_’ means to communicate to the public or distribute.


## 2. Applicability

**2.1.** This _Licence_ governs the use, copying, modification, _Conveying_ of _Covered Source_ and _Products_, and the _Making_ of _Products_. By exercising any right granted under this _Licence_, _You_ irrevocably accept these terms and conditions.

**2.2.** This _Licence_ is granted by the _Licensor_ directly to _You_, and shall apply worldwide and without limitation in time.

**2.3.** _You_ shall not attempt to restrict by contract or otherwise the rights granted under this _Licence_ to other _Licensees_.

**2.4.** This _Licence_ is not intended to restrict fair use, fair dealing, or any other similar right.


## 3. Copying, Modifying and Conveying Covered Source

**3.1.** _You_ may copy and _Convey_ verbatim copies of _Covered Source_, in any medium, provided _You_ retain all _Notices_.

**3.2.** _You_ may modify _Covered Source_, other than _Notices_, provided that _You_ irrevocably undertake to make that modified _Covered Source_ available from a _Source Location_ should _You_ _Convey_ a _Product_ in circumstances where the recipient does not otherwise receive a copy of the modified _Covered Source_. In each case subsection **3.3** shall apply.

_You_ may only delete _Notices_ if they are no longer applicable to the corresponding _Covered Source_ as modified by _You_ and _You_ may add additional _Notices_ applicable to _Your_ modifications. **Including _Covered Source_ in a larger work is modifying the _Covered Source_, and the larger work becomes modified _Covered Source_.**

**3.3.** _You_ may _Convey_ modified _Covered Source_ (with the effect that _You_ shall also become a _Licensor_) provided that _You_:
1. retain _Notices_ as required in subsection **3.2**;
2. add a _Notice_ to the modified _Covered Source_ stating that _You_ have modified it, with the date and brief description of how _You_ have modified it;
3. add a _Source Location_ _Notice_ for the modified _Covered Source_ if _You_ _Convey_ in circumstances where the recipient does not otherwise receive a copy of the modified _Covered Source_; and
4. license the modified _Covered Source_ under the terms and conditions of this _Licence_ (or, as set out in subsection **8.3**, a later version, if permitted by the licence of the original _Covered Source_). Such modified _Covered Source_ must be licensed as a whole, but excluding _Available Components_ contained in it, which remain licensed under their own applicable licences.


## 4. Making and Conveying Products

**4.1.** _You_ may _Make_ _Products_, and/or _Convey_ them, provided that _You_ either provide each recipient with a copy of the _Complete Source_ or ensure that each recipient is notified of the _Source Location_ of the _Complete Source_. That _Complete Source_ **is** _Covered Source_, and _You_ must accordingly satisfy _Your_ obligations set out in subsection **3.3**. If specified in a _Notice_, the _Product_ must visibly and securely display the _Source Location_ on it or its packaging or documentation in the manner specified in that _Notice_.


## 5. Research and Development

**5.1.** _You_ may _Convey_ _Covered Source_, modified _Covered Source_ or _Products_ to a legal entity carrying out development, testing or quality assurance work on _Your_ behalf provided that the work is performed on terms which prevent the entity from both using the _Source_ or _Products_ for its own internal purposes and _Conveying_ the _Source_ or _Products_ or any modifications to them to any person other than _You_. Any modifications made by the entity shall be deemed to be made by _You_ pursuant to subsection **3.2**.


## 6. DISCLAIMER AND LIABILITY

**6.1.** _DISCLAIMER OF WARRANTY_  
The _Covered Source_ and any _Products_ are provided “as is” and any express or implied warranties, including, but not limited to, implied warranties of merchantability, of satisfactory quality, non-infringement of third party rights, and fitness for a particular purpose or use are disclaimed in respect of any _Source_ or _Product_ to the maximum extent permitted by law. The _Licensor_ makes no representation that any _Source_ or _Product_ does not or will not infringe any patent, copyright, trade secret or other proprietary right. The entire risk as to the use, quality, and performance of any Source or _Product_ shall be with _You_ and not the _Licensor_. This disclaimer of warranty is an essential part of this _Licence_ and a condition for the grant of any rights granted under this _Licence_.

**6.2.** _EXCLUSION AND LIMITATION OF LIABILITY_  
The _Licensor_ shall, to the maximum extent permitted by law, have no liability for direct, indirect, special, incidental, consequential, exemplary, punitive or other damages of any character including, without limitation, procurement of substitute goods or services, loss of use, data or profits, or business interruption, however caused and on any theory of contract, warranty, tort (including negligence), product liability or otherwise, arising in any way in relation to the _Covered Source_, modified _Covered Source_ and/or the _Making_ or _Conveyance_ of a _Product_, even if advised of the possibility of such damages, and _You_ shall hold the _Licensor(s)_ free and harmless from any liability, costs, damages, fees and expenses, including claims by third parties, in relation to such use.


## 7. Patents

**7.1.** Subject to the terms and conditions of this _Licence_, each _Licensor_ hereby grants to _You_ a perpetual, worldwide, non‑exclusive, no‑charge, royalty‑free, irrevocable (except as stated in subsections **7.2** and **8.4**) patent license to _Make_, have _Made_, use, offer to sell, sell, import, and otherwise transfer the _Covered Source_ and _Products_, where such licence applies only to those patent claims licensable by such _Licensor_ that are necessarily infringed by exercising rights under the _Covered Source_ as _Conveyed_ by that _Licensor_.

**7.2.** If _You_ institute patent litigation against any entity (including a cross‑claim or counterclaim in a lawsuit) alleging that the _Covered Source_ or a _Product_ constitutes direct or contributory patent infringement, or _You_ seek any declaration that a patent licensed to _You_ under this _Licence_ is invalid or unenforceable then any rights granted to _You_ under this _Licence_ shall terminate as of the date such process is initiated.


## 8. General

**8.1.** If any provisions of this _Licence_ are or subsequently become invalid or unenforceable for any reason, the remaining provisions shall remain effective.

**8.2.** _You_ shall not use any of the name (including acronyms and abbreviations), image, or logo by which the _Licensor_ or CERN is known, except where needed to comply with section **3**, or where the use is otherwise allowed by law. Any such permitted use shall be factual and shall not be made so as to suggest any kind of endorsement or implication of involvement by the _Licensor_ or its personnel.

**8.3.** CERN may publish updated versions and variants of this _Licence_ which it considers to be in the spirit of this version, but may differ in detail to address new problems or concerns. New versions will be published with a unique version number and a variant identifier specifying the variant. If the _Licensor_ has specified that a given variant applies to the _Covered Source_ without specifying a version, _You_ may treat that _Covered Source_ as being released under any version of the CERN-OHL with that variant. If no variant is specified, the _Covered Source_ shall be treated as being released under CERN‑OHL‑S. The _Licensor_ may also specify that the _Covered Source_ is subject to a specific version of the CERN-OHL or any later version in which case _You_ may apply this or any later version of CERN-OHL with the same variant identifier published by CERN.

**8.4.** This _Licence_ shall terminate with immediate effect if _You_ fail to comply with any of its terms and conditions.

**8.5.** However, if _You_ cease all breaches of this _Licence_, then _Your_ _Licence_ from any _Licensor_ is reinstated unless such _Licensor_ has terminated this _Licence_ by giving _You_, while _You_ remain in breach, a notice specifying the breach and requiring _You_ to cure it within 30 days, and _You_ have failed to come into compliance in all material respects by the end of the 30 day period. Should _You_ repeat the breach after receipt of a cure notice and subsequent reinstatement, this _Licence_ will terminate immediately and permanently. Section **6** shall continue to apply after any termination.

**8.6.** This _Licence_ shall not be enforceable except by a _Licensor_ acting as such, and third party beneficiary rights are specifically excluded.



_____________________________________________________________

> TRADUCTION NON‑OFFICIELLE EN FRANÇAIS.  
> Par Lilian Tribouilloy et non approuvé par le CERN  
> **ATTENTION :** Cette traduction n’a pas de valeur juridique. Elle a simplement pour but d’aider à la compréhension des francophones.  
> **ATTENTION :** L’absence d’erreur de traduction ou l’absence de faute d’orthographe ne sont pas garantit.


# CERN Open Hardware License Version 2 - Fortement réciproque


## Préambule

Le CERN a développé cette licence pour promouvoir la collaboration entre concepteurs de matériel et pour fournir un outil juridique qui prend en charge la liberté d’utiliser, d’étudier, de modifier, de partager et de distribuer des designs d’objets et des _Produits_ basés sur ces designs d’objets. La version 2 de la licence CERN Open Hardware est disponible en trois variantes: CERN‑OHL‑P (permissive); et deux licences réciproques: CERN‑OHL‑W (faiblement réciproque) et la présente _Licence_, CERN‑OHL‑S (fortement réciproque).

Le CERN‑OHL‑S est protégé par le droit d’auteur © CERN 2020. Tout le monde est invité à l’utiliser, en forme non modifiée uniquement.

L’utilisation de cette _Licence_ n’implique aucunement l’approbation par le CERN sur aucun _Concédant_ de la _Licence_ ni sur leurs designs ; et le CERN est aucunement partie prennante sur leur développement.


## 1. Définitions

**1.1.** ‹_Licence_› signifie la présente CERN-OHL-**S**.

**1.2.** ‹_Licence Compatible_› signifie
1. toute version antérieure de la licence CERN Open Hardware, ou
2. toute version du CERN‑OHL‑S, ou
3. toute licence qui _Vous_ permet de traiter la _Source_ à laquelle elle s’applique comme étant sous licence CERN‑OHL‑S à condition que lors de la _Transmission_ d’une telle _Source_ ou de tout _Produit_ associé que _Vous_ traitez la _Source_ en question comme étant sous licence CERN‑OHL‑S.

**1.3.** ‹_Source_› signifie des informations telles que les matériaux de conception ou le code qui peuvent être appliqués pour _Fabriquer_ ou tester un _Produit_ ou pour préparer un _Produit_ en vue de son utilisation, de sa _Transmission_ ou de sa vente, quel que soit son support ou la manière dont il est exprimé. Cela peut inclure des _Notifications_.

**1.4.** ‹_Source Couverte_› signifie la _Source_ qui est explicitement mise à disposition sous cette _Licence_.

**1.5.** ‹_Produit_› désigne tout appareil, composant, travail ou objet physique, soit sous forme finie ou intermédiaire, résultant de l’utilisation, l’application ou le traitement de la _Source Couverte_.

**1.6.** ‹_Fabriquer_› signifie créer ou configurer quelque chose, que ce soit par _Fabrication_, assemblage, compilation, chargement ou mise en œuvre de la _Source Couverte_ ou d’un autre _Produit_ ou de tout autre chose.

**1.7.** ‹_Composant Disponible_› désigne toute pièce, sous-assemblage, bibliothèque ou code qui:
1. _Vous_ est concédé en tant que _Source Complète_ sous une _Licence Compatible_ ; ou
2. est disponible, au moment où un _Produit_ ou la _Source_ le contenant est _Transmis_ pour la première fois, à _Vous_ et à tout autres potentiels _Licenciés_ :
	- **en tant que partie physique** avec des droits suffisants et informations (y compris toute configuration et fichiers de programmation et informations sur son caractéristiques et interfaces) pour lui permettre soit d’être _Fabriqué_ lui-même, ou d’être sourcé et utilisé pour _Faire_ le _Produit_ ; ou
	- dans le cadre de la distribution normale d’un outil utilisé pour concevoir ou _Fabriquer_ le _Produit_.

**1.8.** ‹_Source Complète_› signifie l’ensemble de toutes les _Sources_ nécessaires pour créer un _Produit_, sous la forme préférée pour apporter des modifications, y compris les informations d’installation et d’interfaçage nécessaires à la fois pour le _Produit_ et pour tous les _Composants Disponibles_ inclus. Si le format est propriétaire, il doit également être mis à disposition dans un format (si l’outil propriétaire peut le créer) qui est visible avec un outil disponible pour les _Licenciés_ potentiels et sous une licence approuvée par la Free Software Foundation (<https://www.fsf.org/>) ou Open Source Initiative (<https://opensource.org/>). La _Source Complète_ n’a pas besoin d’inclure la source de tous les _Composants Disponibles_, à condition que _Vous_ incluez dans la _Source Complète_ des informations suffisantes pour permettre à un destinataire de créer ou de s’approvisionner et d’utiliser le _Composant Disponible_ pour _Fabriquer_ le _Produit_.

**1.9.** ‹_Emplacement Source_› désigne un emplacement où un _Concédant_ a placé la _Source Couverte_, et que ce _Concédant_ croit raisonnablement rester facilement accessible pendant au moins trois ans pour quiconque veut obtenir une copie numérique.

**1.10.** ‹_Notification_› désigne les avis de droit d’auteur, d’actes juridiques légals et de marque déposée, les références de l’_Emplacement Source_, les avis de modification (sous-section **3.3(2)**) et tous les avis qui se réfèrent à cette _Licence_ et à la clause de non‑responsabilité des garanties incluses dans la _Source Couverte_.

**1.11.** ‹_Licencié_› ou ‹_Vous_› désigne toute personne exerçant des droits en vertu cette _Licence_.

**1.12.** ‹_Concédant_›, une personne physique ou morale qui crée ou modifie la _Source Couverte_. Une personne peut être _Licencié_ et _Concédant_ en même temps.

**1.13.** ‹_Transmettre_› signifie communiquer au public ou distribuer. ‹_Transmission_› signifie l’action de _Transmettre_ dans selon la même signification.


## 2. Applicabilité

**2.1.** Cette licence régit l’utilisation, la copie, la modification, la _Transmission_ de la _Source Couverte_ et des _Produits_, et la _Fabrication_ des _Produits_. En exerçant tout droit accordé en vertu de cette _Licence_, _Vous_ acceptez ces termes et conditions.

**2.2.** Cette _Licence_ vous est concédée directement par le _Concédant_, et s’applique dans le monde entier et sans limitation dans le temps.

**2.3.** _Vous_ ne devez pas tenter de restreindre par contrat, ou par un autre moyen, les droits accordés en vertu de cette _Licence_ à d’autres _Licenciés_.

**2.4.** Cette licence n’est pas destinée à restreindre l’utilisation loyale, l’utilisation équitable, ou tout autre droit similaire.


## 3. Copie, Modification et Transmission de la Source Couverte

**3.1.** _Vous_ pouvez copier et _Transmettre_ des copies textuelles de la _Source Couverte_, en tout support, à condition que vous conserviez tous les _Notifications_.

**3.2.** _Vous_ pouvez modifier la _Source Couverte_, autre que les _Notifications_, à condition que _Vous_ vous engagez irrévocablement à rendre cette _Source Couverte_ modifiée disponible à partir d’un _Emplacement Source_, à condition que _Vous_ _Transmettez_ un _Produit_ dans des circonstances où le destinataire ne reçoit pas autrement une copie de la _Source Couverte_ modifiée. Dans chaque cas, la sous-section **3.3** doit s’appliquer.

_Vous_ ne pouvez supprimer les _Notifications_ que si elles ne s’appliquent plus à la _Source Couverte_ correspondante telle que modifiée par _Vous_. Et _Vous_ pouvez ajouter des _Notifications_ supplémentaires applicables à _Vos_ modifications. **Inclure la _Source Couverte_ dans un travail plus vaste modifie la _Source Couverte_, et l’œuvre plus grande devient une _Source Couverte_ modifiée.**

**3.3.** _Vous_ pouvez _Transmettre_ une _Source Couverte_ modifiée (avec pour effet que _Vous_ deviendrez également un _Concédant_) à condition que _Vous_:
1. conserver les _Notifications_ comme l’exige la sous-section **3.2** ;
2. ajouter une _Notification_ à la _Source Couverte_ modifiée indiquant que vous l’avez modifié, avec la date et une brève description de la façon dont _Vous_ l’avez modifié ;
3. ajouter une _Notification_ de l’_Emplacement Source_ de la _Source Couverte_ modifiée si vous communiquez dans des circonstances où le destinataire ne peut pas autrement recevoir une copie de la _Source Couverte_ modifiée ; et
4. octroyer une licence à la _Source Couverte_ modifiée selon les termes et conditions de cette _Licence_ (ou, comme indiqué dans la sous-section **8.3**, une version ultérieure, si le permet la licence originale de la _Source Couverte_). Cette _Source Couverte_ modifiée doit être, dans son ensemble, sous cette _Licence_, à l’exclusion cependant des _Composants Disponibles_ contenus dans celui-ci, qui eux restent sous leurs propres licences applicables.


## 4. Fabrication et Transmission des Produits

**4.1.** _Vous_ pouvez _Fabriquer_ des _Produits_ et / ou les _Transmettre_, à condition que _Vous_ fournissiez à chaque destinataire une copie de la _Source Complète_ ou que _Vous_ vous assuriez que chaque destinataire est informé de l’_Emplacement Source_ de la _Source Complète_. Cette _Source Complète_ **est** la _Source Couverte_, et vous devez satisfaire en conséquence _Vos_ obligations énoncées au paragraphe **3.3**. Si spécifié dans une _Notification_, il doit être afficher de manière visible et sécurisée l’_Emplacement de la Source_ sur le _Produit_, son emballage ou sa documentation, de la manière spécifiée dans cette _Notification_.


## 5. Recherche et Développement

**5.1.** _Vous_ pouvez _Transmettre_ la _Source Couverte_, la _Source Couverte_ modifiée ou les _Produits_ à une personne morale effectuant le développement, les tests ou l’assurance qualité travaillant en _Votre_ nom à condition que le travail soit effectué selon des conditions qui empêchent l’entité en question d’utiliser à la fois la _Source_ ou les _Produits_ pour ses propres fins internes. Et à condition que la _Transmission_ de la _Source_ ou des _Produits_ ou de toutes les modifications ne soit rapportées à aucune autre personne que _Vous_. Toute modification faite par l’entité sera réputée avoir été faite par _Vous_ conformément à la sous-section **3.2**.


## 6. EXCLUSION DE RESPONSABILITÉ ET RESPONSABILITÉ

**6.1.** _EXCLUSION DE GARANTIE_  
La _Source Couverte_ et tous les _Produits_ sont fournis «tels quels» et toute garantie expresse ou implicite, y compris, mais sans s’y limiter, les garanties implicites de qualité marchande, de qualité satisfaisante, de non‑violation des droits tiers et d’adéquation à un usage particulier ou l’utilisation est refusée à l’égard de toute _Source_ ou _Produit_ dans la mesure maximale autorisée par la loi. Le _Concédant_ ne fait aucune déclaration selon laquelle aucune _Source_ ou _Produit_ n’enfreindre de brevet, de droit d’auteur, de secret commercial ou autre droit de propriété. L’ensemble du risque quant à l’utilisation, la qualité et la performance de toute _Source_ ou _Produit_ incombe à _Vous_ et non au _Concédant_. Cette exclusion de garantie est une partie essentielle de cette _Licence_ et une condition pour l’octroi de tout droit accordée en vertu de cette _Licence_.

**6.2.** _EXCLUSION ET LIMITATION DE RESPONSABILITÉ_  
Le _Concédant_ n’aura aucune responsabilité, dans la mesure maximale autorisée par la loi, pour : les dommages directs, indirects, spéciaux, accessoires, consécutifs, exemplaires, punitifs ou autres de quelque nature que ce soit. Y compris, sans s’y limiter, sur : l’approvisionnement de biens ou services de remplacement, la perte de l’usage ou des données ou des bénéfices, ou sur l’interruption d’activité, quelle qu’en soit la cause et selon toute théorie du contrat, garantie, délit (y compris négligence), responsabilité du fait des _Produits_ ou autre, découlant de quelque manière que ce soit par rapport à la _Source Couverte_, _Source Couverte modifiée_ et / ou la _Fabrication_ ou la _Transmission_ d’un _Produit_, même si _Vous_ êtes informé la possibilité de tels dommages. _Vous_ tiendrez le _Concédant(s)_ libre(s) de toute responsabilité, coûts, dommages, frais et dépenses, y compris les réclamations par des tiers, en relation avec leur utilisation.


## 7. Brevets

**7.1.** Sous réserve des termes et conditions de cette _Licence_, chaque _Concédant_ _Vous_ accorde par la présente une licence de brevet perpétuelle, mondiale, non‑exclusive, gratuite, libre de droits, et irrévocable (sauf stipulé dans les paragraphes **7.2** et **8.4**). Et ceci, pour que _Vous_ puissiez _Fabriquer_, faire _Fabriquer_, utiliser, proposer à la vente, vendre, importer, _Transmettre_ ou autre, la _Source Couverte_ et les _Produits_. Mais à condition qu’une telle licence s’applique uniquement aux revendications de brevet pouvant être attribuées par un tel _Concédant_. Sans quoi le brevet attribué au _Concédant_ serait nécessairement enfreint en exerçant des droits au titre de la _Source Couverte_ comme _Transmise_ par ce _Concédant_.

**7.2.** Si _Vous_ intentez un litige en matière de brevets contre une entité (y compris une demande reconventionnelle ou une contre‑demande dans le cadre d’un procès) alléguant que la _Source Couverte_ ou un _Produit_ constitue un élément direct ou contributif à une violation de brevet. Ou si _Vous_ cherchez à déclarer qu’il y a un brevet, qui _Vous_ est attribué, à l’intérieur de cette _Licence_, rendant ainsi cette _Licence_ invalide ou inapplicable. Alors tous les droits, qui _Vous_ sont accordés en vertu de cette _Licence_, prennent fin à la date à laquelle ce processus est lancé.


## 8. Généralité

**8.1.** Si des dispositions de cette _Licence_ sont ou deviennent ultérieurement invalides ou inapplicables pour quelque raison que ce soit, le reste les dispositions restent en vigueur.

**8.2.** _Vous_ ne devez utiliser aucun nom (y compris les acronymes et abréviations), image ou logo par lequel le _Concédant_ ou le CERN est connu, sauf lorsque cela est nécessaire pour se conformer à l’article **3**, ou lorsque l’utilisation est par ailleurs autorisée par la loi. Une telle utilisation autorisée doit être factuelle et ne doit pas être faite de manière à suggérer une quelconque approbation ou l’implication du _Concédant_ ou de son personnel.

**8.3.** Le CERN peut publier des versions mises à jour et des variantes de cette _Licence_ qu’il considère être dans l’esprit de cette version, mais peut diffèrer dans le détail pour traiter de nouveaux problèmes ou préoccupations. Les nouvelles versions seront publiées avec un numéro de version unique et un identifiant de variante spécifiant la variante. Si le _Concédant_ a spécifié qu’une variante donnée s’applique à la _Source Couverte_ sans spécifier quelle version, vous pouvez considérer cette _Source Couverte_ comme étant publié sous n’importe quelle des versions du CERN‑OHL avec cette variante. Si aucune variante n’est spécifiée, la _Source Couverte_ sera considérée comme étant placé sous la CERN‑OHL‑S. Le _Concédant_ peut préciser également que la _Source Couverte_ est soumise à un version du CERN-OHL ou toute version ultérieure, auquel cas _Vous_ pouvez appliquer cette version ou toute version ultérieure de la CERN-OHL avec le même identifiant de variante publié par le CERN.

**8.4.** Cette _Licence_ prend fin avec effet immédiat si _Vous_ fautez sur la conformité à l’un des termes ou à l’une des conditions.

**8.5.** Cependant, si vous cessez toutes les violations de cette _Licence_, alors _Votre_ _Licence_ accordée par tout _Concédant_ est rétablie. À moins que ce _Concédant_ n’ait a mis fin à cette _Licence_ en _Vous_ donnant, pendant que _Vous_ restez en violation, une _Notification_ précisant la violation et _Vous_ demandant d’y remédier dans les 30 jours, et que _Vous_ ne vous êtes pas conformé à tous égards importants, à la fin de la période de 30 jours. Si _Vous_ répétez la violation après avoir reçu une _Notification_ de réparation et une réintégration subséquente, cette _Licence_ prendra fin immédiatement et définitivement. La section **6** continue de s’appliquer après toute résiliation.

**8.6.** Cette _Licence_ ne sera exécutoire que par un _Concédant_ agissant en tant que tel, et les droits des tiers bénéficiaires sont spécifiquement exclus.

